$(function () {
    $.nette.init();// And you fly...

    /*
    $("body").on("keypress","form#frm-ticketForm input#frm-ticketForm-discount-discountPercent",function(e){
        if (e.which == 13) {
            console.log( $("form#frm-ticketForm input#discount-button") );
            $("form#frm-ticketForm input#discount-button").submit();
            return false; //<---- Add this line
        }
    });*/

    //ve formulari se vstupenkou, na klavesu enter, nic nedelat
    $("body").on("keypress","form#frm-ticketForm input:text",function(e){
        if (e.which == 13) {
            return false;//<---- Add this line
        }
    });

    $("body").on( "change", "input[type='radio'].claculate-price", function() {
        //console.log("body hehehe: " + $(this).val() + ": " +  getRadioPrice($(this)) + ", (classic: " + ticlet_classic + ", vip: " + ticlet_vip + ")" );
        console.log( "totalPrice: " + getTotalPriceRadio() );
        $("#total-price").text(getTotalPriceRadio());

    });

    /**
     * Ziskej cenu za aktualne zaskrtnuty radio
     * @param radio
     * @returns {*}
     */
    function getRadioPrice(radio) {
        if(radio.val()=="classic") {
            return ticlet_classic;
        } else if(radio.val()=="vip") {
            return ticlet_vip;
        }
    }

    function getTotalPriceRadio() {
        totalPrice = 0;
        $("body input:radio:checked.claculate-price").each(function(){
            totalPrice+=getRadioPrice($(this));
        });
        return totalPrice;
    }

});