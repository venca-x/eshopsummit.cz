<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

        $router[] = new Route( '/', array(
            'presenter' => 'Homepage',
            'action' => 'default',
        ));
        //), Route::SECURED );
        /*
        $router[] = new Route( '/', array(
            'presenter' => 'Homepage',
            'action' => 'fake',
        ));
        //), Route::SECURED );

        $router[] = new Route( '/new/', array(
            'presenter' => 'Homepage',
            'action' => 'default',
        ));
        //), Route::SECURED );
*/
        $router[] = new Route( '/predprodej-vstupenek', array(
            'presenter' => 'Form',
            'action' => 'default',
        ));
        //), Route::SECURED );

        $router[] = new Route( '/predprodej-videi', array(
            'presenter' => 'VideoForm',
            'action' => 'default',
        ));

        $router[] = new Route('/admin/<presenter>/<action>[/<id>]', array(
            'module' => 'Admin',
            'presenter' => 'Homepage',
            'action' => 'default',
        ));
        //), Route::SECURED );

        $router[] = new Route('<action>', array(
            'presenter' => 'Homepage',
            'action' => array(
                Route::VALUE => 'default',
                Route::FILTER_TABLE => array(
                    'pripadove-studie' => 'studies',
                    'nas-team' => 'team',
                    'kontakt' => 'contact',
                    'pravidla-a-podminky' => 'conditions',
                    'jak-se-stat-partnerem' => 'howToPartner',
                ),
            ),
            'id' => NULL,
        ));
        //), Route::SECURED );

		$router[] = new Route( '/stav-platby/<token>', array(
			'presenter' => 'Contribution',
			'action' => 'state',
		));

        $router[] = new Route( '/stav-platby/bankovni-prevod/<token>', array(
            'presenter' => 'Form',
            'action' => 'bankTransfer',
        ));

		$router[] = new Route('<presenter>/<action>[/<id>]', array(
			'presenter' => array(
				Route::VALUE => 'Homepage',
				Route::FILTER_TABLE => array(
					//'novinky' => 'Article',
				),
			),
			'action' => array(
				Route::VALUE => 'default',
				Route::FILTER_TABLE => array(
					'o-konferenci' => 'about',
					'nas-team' => 'team',
					'kontakt' => 'contact',
					'pravidla-a-podminky' => 'conditions',
					'ochrana-osobnich-udaju' => 'protection',
				),
			),
			'id' => NULL,
		));
		//), Route::SECURED);

		return $router;
	}

}
