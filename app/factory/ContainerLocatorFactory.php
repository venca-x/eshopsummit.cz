<?php

namespace App\Factory;
use Nette;

/**
 * Tovarna pro ziskani parametru z configu
 * @author vEnCa-X
 */
class ContainerLocatorFactory extends Nette\Object
{

    /** @var array */
    private $parameters;
    
    private $httpRequest;

    /**
     * @param Nette\DI\Container $systemContainer
     */
    public function __construct( Nette\DI\Container $systemContainer )
    {
        $this->parameters = $systemContainer->getParameters();
        $this->httpRequest = $systemContainer->getService('httpRequest');
    }

    /**
     * Ziskej parametry z configu
     * @param array $parameter
     * @return array|bool
     */
    public function getParameters( $parameter = array() )
    {
        $value = array();
        $i = 1;
        foreach ( $parameter as $key )
        {
            if ( $i == 1 )
            {
                $value = isset( $this->parameters[$key] ) ? $this->parameters[$key] : FALSE;
            }
            else
            {
                $value = $value[$key];
            }
        }
        return $value;
    }
    
    /**
     * Ziskej parametry v URL adrese
     * @return array
     */
    public function getHttpQuery()
    {
        return $this->httpRequest->getQuery();
    }

    /**
     * Ziskej klientovu IP
     * @return string
     */
    public function getRemoteAddress()
    {
        return $this->httpRequest->getRemoteAddress();
    }

}
