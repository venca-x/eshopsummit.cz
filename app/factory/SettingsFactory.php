<?php

namespace App\Factory;

use App;
use App\Factory;
use App\Model;
use Nette\Mail\Message;
use Nette;
use Exception;


class SettingsFactory extends Nette\Object
{

    /**
     * Vrat variablini symol pro fakturu / proformu - objednavka vstupenek
     * @param $visitorId
     * @return string
     */
    public static function getUserVariableSymbol( $visitorId ) {
        return "16" . str_pad( $visitorId, 8, "0", STR_PAD_LEFT );
    }


    /**
     * Vrat variablini symol pro fakturu / proformu - objednavka videi
     * @param $videoId
     * @return string
     */
    public static function getUserVariableSymbolVideo($videoId ) {
        return "16" . str_pad( (5000 + $videoId), 8, "0", STR_PAD_LEFT );
    }

}