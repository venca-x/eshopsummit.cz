<?php

namespace App\Factory;

use App;
use App\Factory;
use Nette;
use Nette\Utils\DateTime;


class DateFactory extends Nette\Object
{

    /**
     * Zkoncil jiz prodej vstupenek
     * @return bool
     */
    public function isNowAfterTicketShop() {
        if(new DateTime() >= $this->getEnndOfTicketShop()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Datum di kdy je mozne koupit vstuoenku
     * @return DateTime
     */
    public function getEnndOfTicketShop() {
       return new DateTime("2016-03-30 22:00:00");
    }

}