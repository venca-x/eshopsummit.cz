<?php

namespace App\Factory;

use App;
use App\Factory;
use App\Model;
use Nette\Mail\Message;
use Nette;
use Exception;
use Latte;
use Nette\Mail\SendmailMailer;


class EmailFactory extends Nette\Object
{

    /** @var Nette\Mail\IMailer */
    private $mailer;

    protected $defaultEmailFrom = 'E-shop summit <objednavky@eshopsummit.cz>';

    public function __construct()
    {
        $this->mailer = $this->getSmtpMailer();
    }

    /**
     * Get SmtpMailer
     * @return Nette\Mail\SmtpMailer
     */
    private function getSmtpMailer()
    {

        //$mailer = new \Nette\Mail\SmtpMailer(array(
        $mailer = new \Nette\Mail\SmtpMailerOptions(array(
            'host' => '185.25.184.232',
            'port' => 587,
            'username' => 'objednavky@eshopsummit.cz',
            'password' => 'szQW2l5AEcrh8av5DznZPhVoHqhnUKSD',
            'secure' => 'tls',
            'options' => array (
                'ssl' => array(
                    'verify_peer'  => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                )
            )
        ));

        //$mailer = new SendmailMailer;//docasne, proc nefugnuje to nad tim
        return $mailer;
    }

    /**
     * @param Nette\Database\Table\ActiveRow $visitorRow
     * @param $cardPayLink
     * @throws Exception
     * @throws \Throwable
     */
    public function sendUrgenceEmail7Day(Nette\Database\Table\ActiveRow $visitorRow, $cardPayLink) {

        $latte = new Latte\Engine;
        $subject = "Tvá vstupenka na #ESS16 stále není uhrazena! Víš to?";

        $params = array(
            'visitorRow' => $visitorRow,
        );

        if($visitorRow->payments == "payment_bank_transfer") {
            //bankovnim prevodem
            $text = $latte->renderToString( "./../app/presenters/templates/email/urgence7daysBank.latte", $params);
        } else {
            //platba kartou

            $params["cardPayLink"] = $cardPayLink;//$this->link("//Contribution:payment", $visitorRow->token);
            $text = $latte->renderToString( "./../app/presenters/templates/email/urgence7daysCard.latte", $params);
        }
/*
        echo "Email pro: " . $visitorRow->invoice_email . "<br/>";
        echo "Subject: " . $subject . "<br/>";
        echo "Text: " . $text . "";;
        echo "<hr/>";*/

        $this->sendEmail(NULL, $visitorRow->invoice_email, $subject, $text);
    }

    /**
     * Send EMAIL
     * @param null $from
     * @param $to
     * @param $subject
     * @param $text html is allowed
     * @param $attachement attachement with path
     */
    public function sendEmail( $from = NULL, $to, $subject, $text, $attachements = NULL )
    {

        if( $from == NULL )
        {
            //set default from
            $from = $this->defaultEmailFrom;
        }

        $mail = new \Nette\Mail\Message;
        $mail->setFrom( $from )
            ->addTo( $to )
            ->setSubject( $subject )
            ->setHtmlBody( $text );
        if( $attachements != NULL ) {
            //$mail->addAttachment($attachements);
            foreach( $attachements as $attachement ) {
                $mail->addAttachment($attachement);
            }
        }

        $this->mailer->send($mail);
    }

}