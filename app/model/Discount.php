<?php

namespace App\Model;

class Discount extends Repository
{

    /** @var string */
    protected $tableName = "discount";

    /**
     * metoda vraci fetchPairs
     * @return array
     */
    public function getFP()
    {
        return $this->findAll()->order( 'name' )->fetchPairs( 'id', 'name' );
    }

    public function findActiveByName($name){
        return $this->findOneBy(array( "active" => 1, "name" => $name ));
    }


}
