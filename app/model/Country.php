<?php

namespace App\Model;

class Country extends Repository
{

    /** @var string */
    protected $tableName = "country";

    /**
     * metoda vraci fetchPairs
     * @return array
     */
    public function getFP()
    {
        return $this->findAll()->order( 'name' )->fetchPairs( 'id', 'name' );
    }

}
