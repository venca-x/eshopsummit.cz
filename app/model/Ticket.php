<?php

namespace App\Model;

use Nette\Utils\DateTime;

class Ticket extends Repository
{

    /** @var string */
    protected $tableName = "ticket";

    /**
     * Ziskej klasicke vstupenky
     * @return \Nette\Database\Table\Selection
     */
    public function getAllClassics() {
        return $this->findBy(array("vip" => 0));
    }

    public function getClassicTicketForNow() {
        return $this->findOneBy(array("vip" => 0, "price_from <= ?" => new DateTime(), "price_to >= ?" => new DateTime() ));
    }

    /**
     * Zsikej jendu VIP vstupenku
     * @return FALSE|\Nette\Database\Table\ActiveRow
     */
    public function getVIP()
    {
        return $this->findOneBy(array("vip" => 1));
    }

}
