<?php

namespace App\Model;

use Nette\Utils\DateTime;

class User extends Repository
{

    /** @var string */
    protected $tableName = "user";

    /**
     * Vyhledani uzivatele podle emailu
     * @param type $email
     * @return \Nette\Database\Table\ActiveRow
     */
    public function findByEmail( $email )
    {
        return $this->findOneBy( array("email" => $email) );
    }

}
