<?php

namespace App\Model;

class VideoPaymentTransaction extends Repository
{
    const PENDING = 'pending';
    const PAID = 'paid';
    const CANCELLED = 'cancelled';

    /** @var string */
    protected $tableName = "video_payment_transaction";

}
