<?php

namespace App\Model;


use DateInterval;
use Nette\Utils\DateTime;

class Video extends Repository
{

    /** @var string */
    protected $tableName = "video";

    public function findByToken($token) {
        return $this->findOneBy(array("token" => $token ));
    }

    /**
     * Zaplacene vstupenky
     * @return \Nette\Database\Table\Selection
     */
    public function findAllPaid() {
        return $this->findBy(array("paid" => 1 ));
    }

    /**
     * Vrat uzivatele kteri nemaji zplaceno aleson 7 dni a jeste jim nebyla poslana urgence
     * @return \Nette\Database\Table\Selection
     */
    public function getVisitorsWithoutPayment7Days() {
        return $this->getVisitorsWithoutPayment($this->getDateTimeBefore7Days());
    }

    /**
     * Najdi email vsech: zaregistrovanych
     */
    public function getEmailsInvoiceAndVisitors() {
        return $this->query( "(SELECT invoice_email AS email FROM visitor)
                                UNION DISTINCT
                                (SELECT email FROM visitor_ticket)");
    }

    /**
     * Vrat uzivatele kteri nemaji zplaceno aleson X dni a jeste jim nebyla poslana urgence
     * @param DateTime $dateTimeFrom
     * @return \Nette\Database\Table\Selection
     */
    protected function getVisitorsWithoutPayment(DateTime $dateTimeFrom = null ) {
        $selection = $this->findBy(array( "paid" => 0, "urgence_email_7_day IS NULL" ));

        if( $dateTimeFrom != null )
        {
            $selection->where( "registered < ?", $dateTimeFrom );
        }

        return $selection;
    }

    /**
     * Ziskej datum pred 7 dny
     * @return DateTime
     */
    private function getDateTimeBefore7Days()
    {
        $date = new DateTime();
        $date->sub(new DateInterval('P7D'));

        return $date;
    }

}
