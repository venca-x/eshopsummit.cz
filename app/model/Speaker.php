<?php

namespace App\Model;

class Speaker extends Repository
{

    /** @var string */
    protected $tableName = "speaker";

    public function findAllOrderBigSort(){
        return $this->findAll()->order("speaker_type_id DESC, sort DESC");
    }

    public function findBigSpeakers(){
        return $this->findBy(array( "speaker_type_id" => 1 ))->order("sort DESC");
    }

    public function findSmallSpeakers(){
        return $this->findBy(array( "speaker_type_id" => 2 ))->order("sort DESC");
    }

    public function findPanelOperationSpeakers(){
        //Panelovka – provoz
        return $this->findBy(array( "speaker_type_id" => 3 ))->order("sort DESC");
    }

    public function findPanelMarketingSpeakers(){
        //Panelovka – marketing
        return $this->findBy(array( "speaker_type_id" => 4 ))->order("sort DESC");
    }

    public function findCaseStudySpeakers(){
        //Případovky
        return $this->findBy(array( "speaker_type_id" => 5 ))->order("sort DESC");
    }

    public function getProgramTo($date, $fromTime, $toTime) {
        return $this->findBy( array( "date" => $date, "time>=?" => $fromTime, "time<?" => $toTime) )->order("time ASC");
    }

}
