<?php

namespace App\Model;

class PaymentTransaction extends Repository
{
    const PENDING = 'pending';
    const PAID = 'paid';
    const CANCELLED = 'cancelled';

    /** @var string */
    protected $tableName = "payment_transaction";

}
