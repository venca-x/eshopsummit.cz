<?php

namespace App\Model;

class LecturerTip extends Repository
{

    /** @var string */
    protected $tableName = "lecturer_tip";

    public function findAllOrderDate()
    {
        return $this->findAll()->order("date ASC");
    }

}
