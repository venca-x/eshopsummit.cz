<?php

namespace App\Model;

class Interest extends Repository
{

    /** @var string */
    protected $tableName = "interest";

    /**
     * Vyhledani uzivatele podle emailu
     * @param string $email
     * @return \Nette\Database\Table\ActiveRow
     */
    public function findByEmail( $email )
    {
        return $this->findOneBy( array( 'email' => $email ) );
    }

    public function findAllOrderDate()
    {
        //return NULL;
        return $this->findAll()->order("date ASC");
    }

}
