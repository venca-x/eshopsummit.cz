<?php

namespace App\Model;

class Setting extends Repository
{

    /** @var string */
    protected $tableName = "setting";

    /**
     * Nacti prvni radek - jedniny
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getRow()
    {
        return $this->findAll()->fetch();
    }

}
