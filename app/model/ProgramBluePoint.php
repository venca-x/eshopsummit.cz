<?php

namespace App\Model;

class ProgramBluePoint
{

    private $name;
    private $time;

    /**
     * ProgramBluePoint constructor.
     * @param $name
     * @param $time
     */
    public function __construct($name, $time)
    {
        $this->name = $name;
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }




}