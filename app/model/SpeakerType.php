<?php

namespace App\Model;

class SpeakerType extends Repository
{

    /** @var string */
    protected $tableName = "speaker_type";

    /**
     * metoda vraci fetchPairs
     * @return array
     */
    public function getFP()
    {
        return $this->findAll()->order( 'name' )->fetchPairs( 'id', 'name' );
    }

}
