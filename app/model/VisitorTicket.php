<?php

namespace App\Model;

class VisitorTicket extends Repository
{

    /** @var string */
    protected $tableName = "visitor_ticket";

    public function findAllPaid() {
        return $this->findAll()->where('visitor.paid = 1');
    }

    public function findAllClassic() {
        //return $this->findBy(array( "ticket_id != 5" => "" ));
        return $this->findAll()->where('ticket_id != 5');
    }

    public function findAllClassicPaid() {
        //return $this->findBy(array("ticket_id != 5" => "", "visitor.paid" => 1 ));
        return $this->findAll()->where('ticket_id != 5 AND visitor.paid = 1');
    }


    public function findAllVip() {
        //return $this->findBy(array( "ticket_id" => "5" ));
        return $this->findAll()->where('ticket_id = 5');
    }

    public function findAllVipPaid() {
        //return $this->findBy(array("ticket_id" => "5", "visitor.paid" => 1 ));
        return $this->findAll()->where('ticket_id = 5 AND visitor.paid = 1');
    }

    /**
     * Ziskej celkem vybranou sumu
     */
    public function getSumPrice() {
        $sumPrice = 0;
        foreach( $this->findBy(array("visitor.paid" => 1)) as $visitorTicketRow) {
            $sumPrice += $visitorTicketRow->ticket->price - $visitorTicketRow->visitor->discount_price;
        }

        return $sumPrice;
    }

}
