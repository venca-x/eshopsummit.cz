<?php

namespace App\Model;

use App;
use App\Model;
use Nette;
use Nette\Security;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator
{

    /** @var Model\User */
    private $midelUser;

    /**
     * 
     * @param Nette\DI\Container $context
     */
    public function __construct( Model\User $modelUser )
    {
        $this->midelUser = $modelUser;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate( array $credentials )
    {
        list($email, $password) = $credentials;
        //najdi uzivatele
        $row = $this->midelUser->findByEmail( $email );

        if ( !$row )
        {
            throw new Security\AuthenticationException( 'Neexistující email', self::IDENTITY_NOT_FOUND );
        }

        if ( $row->password !== $this->calculateHash( $password ) )
        {
            throw new Security\AuthenticationException( "Špatné heslo", self::INVALID_CREDENTIAL );
        }

        return $this->createIdentity($row);
    }

    /**
     * metoda vytvarejici identitu
     * z identity vzdy odebere heslo
     * @param \Nette\Database\Table\ActiveRow $userObject - objekt uzivatele ktereho bych mel prihlasit
     * @return \Nette\Security\Identity
     */
    private function createIdentity( $userObject )
    {
        $userArray = $userObject->toArray();
        unset( $userArray[ 'password' ] );
        return new Security\Identity( $userObject->id, $userObject->role, $userArray );
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public function calculateHash( $password )
    {
        return hash( 'sha512', $password );
    }

}
