<?php

namespace App\AdminModule\Presenters;

use App;
use App\Model;

abstract class BasePresenter extends \App\Presenters\BasePresenter
{

    public function startup()
    {
        parent::startup();
    }
}
