<?php

namespace App\AdminModule\Presenters;

use App;
use App\Model;

abstract class SecuredPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();

        $this->mustBeAdminLogged();
    }
}
