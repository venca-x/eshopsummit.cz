<?php

namespace App\AdminModule\Presenters;

use App;
use App\Factory;
use App\Model;
use Nette;
use Nette\Application\UI\Form;
use Services;
use Vencax;

class NewsletterPresenter extends SecuredPresenter
{
    const EMAIL_NEWSLETTER = "novinky@eshopsummit.cz";

    /** @var App\Factory\EmailFactory */
    protected $factoryEmail;

    /** @var Model\Interest */
    protected $modelInterest;

    /** @var App\Model\Setting */
    protected $modelSetting;

    /** @var App\Model\Visitor */
    protected $modelVisitor;


    /** @var Nette\Database\Table\ActiveRow */
    protected $setting;

    /**
     * DashboardPresenter constructor.
     * @param Model\Visitor $modelVisitor
     */
    public function __construct(Factory\EmailFactory $factoryEmail,
                                Model\Interest $modelInterest,
                                Model\Setting $modelSetting,
                                Model\Visitor $modelVisitor)
    {
        $this->factoryEmail = $factoryEmail;
        $this->modelInterest = $modelInterest;
        $this->modelSetting = $modelSetting;
        $this->modelVisitor = $modelVisitor;
    }

    public function renderDefault()
    {
        $this->template->aInterest = $this->modelInterest->findAllOrderDate();
    }


    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> newsletter email form
    /**
     * Change password form
     * @return Nette\Application\UI\Form
     */
    protected function createComponentNewsletterEmailForm()
    {
        $form = new Form;

        $form->setRenderer( new Nette\Forms\Rendering\TB3FormRenderer() );

        $form->addText('subject', 'Předmět:')
            ->setRequired('Zadejte prosím předmět')
            ->setAttribute('placeholder', 'Předmět');
        //->setValue( "BarCamp v Český Budějovicích je za námi!" );

        $form->addTextArea('text', 'Text emailu:')
            ->setAttribute( 'rows', 20 )
            ->setRequired('Zadejte prosím text emailu')
            ->setAttribute('placeholder', 'Text emailu, pro formátování a odkazy použij HTML');
        //->setValue("");

        $form->addSubmit('sendSave', 'Uložit text')
            ->setAttribute('class', 'btn btn-warning');

        $form->addSubmit('sendShow', 'Zobrazit naformátovaný email')
            ->setAttribute('class', 'btn btn-warning');

        $form->addSubmit('sendAdmin', 'Odeslat adminům')
            ->setAttribute('onclick', "return confirm('Opravdu odeslat adminům?')")
            ->setAttribute('class', 'btn btn-primary');

        $form->addSubmit('send', 'Odeslat email lidem z newsletteru')
            ->setAttribute('onclick', "return confirm('Opravdu odeslat všem lidem?')")
            ->setAttribute('class', 'btn btn-danger');

        $form->addSubmit('sendRegistered', 'Odeslat email zaregistrovanym lidem')
            ->setAttribute('onclick', "return confirm('Opravdu odeslat všem zaregistrovanym lidem (fakturacni udaje + navstevnici)?')")
            ->setAttribute('class', 'btn btn-danger');

        $settingRow = $this->modelSetting->getRow();
        $form->setDefaults(array(
            'subject' => $settingRow->newsletter_subject,
            'text' => $settingRow->newsletter_text
        ));

        $form->onSuccess[] = array($this, 'newsletterEmailFormSucceeded');
        return $form;
    }

    public function newsletterEmailFormSucceeded( $form, $values )
    {

        //dump( $form );
        //dump( $values );

        if ($form['sendSave']->isSubmittedBy()) {
            $this->saveEmailData($values);

            $this->flashMessage( "Uloženo", "alert-success" );
            $this->redirect( 'this' );
        }

        if ($form['sendShow']->isSubmittedBy()) {
            echo $values["subject"] . "<br/>";
            echo "<hr/>";
            echo nl2br($values["text"]) . "<br/>";
            exit;
        }

        if ($form['sendAdmin']->isSubmittedBy()) {
            $this->saveEmailData($values);

            $this->factoryEmail->sendEmail( self::EMAIL_NEWSLETTER, "vencax@gmail.com", $values['subject'] . " - POUZE ADMINUM", nl2br( $values['text'] ) );
            $this->factoryEmail->sendEmail( self::EMAIL_NEWSLETTER, "jsem@eshopkonzultant.cz", $values['subject'] . " - POUZE ADMINUM", nl2br( $values['text'] ) );

            $this->flashMessage( "Email odeslán adminům", "alert-success" );
            $this->redirect( 'this' );
        }

        if ($form['send']->isSubmittedBy()) {
            $this->saveEmailData($values);

            $aInterest = $this->modelInterest->findAll();//akcni silenci
            foreach( $aInterest as $oInterest )
            {
                $this->factoryEmail->sendEmail( self::EMAIL_NEWSLETTER, $oInterest->email, $values['subject'], nl2br( $values['text'] ) );
            }

            $this->flashMessage( "Email odeslán VŠEM v newsletteru: " . $aInterest->count(), "alert-success" );
            $this->redirect( 'this' );
        }

        if ($form['sendRegistered']->isSubmittedBy()) {
            $this->saveEmailData($values);

            $users = $this->modelVisitor->getEmailsInvoiceAndVisitors();//yaregistrovani lide
            foreach( $users as $userRow )
            {
                if($userRow->email != "" && $userRow->email != " ") {
                    //echo $userRow->email . "<br/>";
                    $this->factoryEmail->sendEmail( self::EMAIL_NEWSLETTER, $userRow->email, $values['subject'], nl2br( $values['text'] ) );
                }
            }

            $this->flashMessage( "Email odeslán VŠEM yaregistrovaným", "alert-success" );
            $this->redirect( 'this' );
        }

        $this->flashMessage( "Nic neposilam", "alert-success" );

        $this->redirect( 'this' );
    }

    private function saveEmailData($values) {
        $settingRow = $this->modelSetting->getRow();
        $settingRow->update( array( "newsletter_subject" => $values["subject"], "newsletter_text" => $values["text"] ) );
    }
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< newsletter
}
