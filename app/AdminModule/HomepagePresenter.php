<?php

namespace App\AdminModule\Presenters;

use App;
use App\Factory;
use App\Model;
use Nette;
use Vencax;
use Nette\Application\UI\Form;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

    /** @var Model\User */
    public $modelUser;

    public function __construct( Model\User $modelInterest)
    {
        $this->modelUser = $modelInterest;
    }

    public function actionDefault()
    {
        if ( $this->getUser()->isLoggedIn() && $this->isAdmin() )
        {
            $this->redirect( ':Admin:Dashboard:default' );
        }
        else if( $this->getUser()->isLoggedIn() )
        {
            //normalni clovek chce jit do administrace
            $this->redirect( ':Homepage:' );
        } else {
            //login page

        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage( 'Hezký den šéfe :-)', "alert-warning" );
        $this->redirect( 'Homepage:default' );
    }

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('email', 'E-mail:')
            ->setRequired('Please enter your e-mail.');
        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');
        $form->addSubmit('send', 'Login');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form, $values)
    {
        try {
            $this->user->login($values->email, $values->password);
            $this->redirect("Dashboard:");
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

}
