<?php

namespace App\AdminModule\Presenters;

use App;
use App\Factory;
use App\Model;
use Nette;
use Nette\Application\UI\Form;
use Services;
use Vencax;

class DashboardPresenter extends SecuredPresenter
{
    /** @var Model\Interest */
    protected $modelInterest;

    /** @var Model\Lecture */
    protected $modelLecture;

    /** @var Model\LecturerTip */
    protected $modelLecturerTip;

    /** @var Model\Setting */
    protected $modelSetting;

    /** @var Model\Speaker */
    protected $modelSpeaker;

    /** @var Model\SpeakerType */
    protected $modelSpeakerType;

    /** @var Model\Video */
    protected $modelVideo;

    /** @var Model\Visitor */
    protected $modelVisitor;

    /** @var Model\VisitorTicket */
    protected $modelVisitorTicket;

    /** @var Nette\Database\Table\ActiveRow */
    protected $visitor;

    /** @var Nette\Database\Table\ActiveRow */
    protected $video;

    /** @var Nette\Database\Table\ActiveRow */
    protected $setting;

    /** @var Nette\Database\Table\ActiveRow */
    protected $speaker;

    /** @var Services\VisitorService */
    public $serviceVisitor;

    /** @var Services\InvoiceCreator */
    public $invoiceCreator;

    /**
     * DashboardPresenter constructor.
     * @param Model\Visitor $modelVisitor
     */
    public function __construct(Model\Interest $modelInterest,
                                Model\Lecture $modelLecture,
                                Model\LecturerTip $modelLecturerTip,
                                Model\Setting $modelSetting,
                                Model\Speaker $modelSpeaker,
                                Model\SpeakerType $modelSpeakerType,
                                Model\Video $modelVideo,
                                Model\Visitor $modelVisitor,
                                Model\VisitorTicket $modelVisitorTicket,
                                Services\InvoiceCreator $invoiceCreator,
                                Services\VisitorService $serviceVisitor)
    {
        $this->modelInterest = $modelInterest;
        $this->modelLecture = $modelLecture;
        $this->modelLecturerTip = $modelLecturerTip;
        $this->modelSetting = $modelSetting;
        $this->modelSpeaker = $modelSpeaker;
        $this->modelSpeakerType = $modelSpeakerType;
        $this->modelVideo = $modelVideo;
        $this->modelVisitor = $modelVisitor;
        $this->modelVisitorTicket = $modelVisitorTicket;
        $this->invoiceCreator = $invoiceCreator;
        $this->serviceVisitor = $serviceVisitor;
    }


    public function startup()
    {
        parent::startup();

        $this->mustBeAdminLogged();
    }

    public function actionUserEdit($id) {

        $this->visitor = $this->modelVisitor->find($id);
        if($this->visitor == FALSE) {
            $this->raise404();
        }
    }


    public function actionUserDelete($id) {

        $this->visitor = $this->modelVisitor->find($id);
        if($this->visitor == FALSE) {
            $this->raise404();
        } else {
            //smazat pujde pouze pokud jeste nema zaplaceno
            if($this->visitor->paid == 0) {
                //nema zaplaceno
                foreach( $this->visitor->related('payment_transaction.visitor_id') as $row){
                    $row->delete();
                }

                foreach( $this->visitor->related('visitor_ticket.visitor_id') as $row){
                    $row->delete();
                }

                $this->visitor->delete();
            } else {
                $this->flashMessage("Nelze smazat uzivatele, ktery ma jiz zaplaceno", "alert-danger");
            }
        }

        $this->redirect("Dashboard:users");
    }

    public function actionSpeakerEdit($id) {

        $this->speaker = $this->modelSpeaker->find($id);
        if($this->speaker == FALSE) {
            $this->raise404();
        }
    }

    public function actionSpeakerDelete($id) {

        $this->speaker = $this->modelSpeaker->find($id);
        if($this->speaker == FALSE) {
            $this->raise404();
        }

        $this->speaker->delete();
        $this->flashMessage("Uživateli jsem nastavil že má zaplaceno a nechal mu vygenerovat fakturu na idoklad.cz", "alert-success");
        $this->redirect("Dashboard:speakers");
    }

    public function actionPartners() {
        $this->setting = $this->modelSetting->getRow();
    }

    public function actionVideoEdit($id) {

        $this->video = $this->modelVideo->find($id);
        if($this->video == FALSE) {
            $this->raise404();
        }
    }


    public function actionVideoDelete($id) {

        $this->video = $this->modelVideo->find($id);
        if($this->video == FALSE) {
            $this->raise404();
        } else {
            //smazat pujde pouze pokud jeste nema zaplaceno
            if($this->video->paid == 0) {
                //nema zaplaceno

                //nema zaplaceno
                foreach( $this->video->related('video_payment_transaction.video_id') as $row){
                    $row->delete();
                }

                $this->video->delete();
            } else {
                $this->flashMessage("Nelze smazat video uzivatele, ktery ma jiz zaplaceno", "alert-danger");
            }
        }

        $this->redirect("Dashboard:videos");
    }

    public function renderDefault() {
        $this->template->newsletterCount = $this->modelInterest->findAll()->count();//pocet lidi v newsletteru
        $this->template->lectureCount = $this->modelLecture->findAll()->count();//pocet Případové studie a příběhy

        $this->template->visitorsPaidCount = $this->modelVisitorTicket->findAllPaid()->count();//pocet uhrazenych navstevniku
        $this->template->visitorsCount = $this->modelVisitorTicket->findAll()->count();//celkovy pocet navstevniku
        $this->template->ticketClassicPaidCount = $this->modelVisitorTicket->findAllClassicPaid()->count();//pocet uhrazenych klasickzch vstupenek
        $this->template->ticketClassicCount = $this->modelVisitorTicket->findAllClassic()->count();//pocet vsech klasickych vstupenek
        $this->template->ticketVipPaidCount = $this->modelVisitorTicket->findAllVipPaid()->count();//pocet uhrazenych VIP vstupenek
        $this->template->ticketVipCount = $this->modelVisitorTicket->findAllVip()->count();//pocet vsech VIP vstupenek
        $this->template->lecturesTipsCount = $this->modelLecturerTip->findAll()->count();//pocet tipu na recnikty

        $this->template->videoCount = $this->modelVideo->findAll()->count();//pocet vsech videi
        $this->template->videoPaidCount = $this->modelVideo->findAllPaid()->count();//pocet zaplacenych videi

        $this->template->sumPrice = $this->modelVisitorTicket->getSumPrice();//pocet tipu na recnikty



    }

    public function renderUsers() {
        $this->template->visitors = $this->modelVisitor->findAll();
    }

    public function renderUsersList() {
        $this->template->visitors = $this->modelVisitor->findAll();
    }

    public function renderUserEdit() {
        $this->template->visitor = $this->visitor;
    }

    public function renderLectureTip() {
        $this->template->lecturerTips = $this->modelLecturerTip->findAll();
    }

    public function renderSpeakers() {
        $this->template->speakers = $this->modelSpeaker->findAllOrderBigSort();
    }

    public function renderLectures() {
        $this->template->lectures = $this->modelLecture->findAll();
    }

    public function renderVideos() {
        $this->template->videos = $this->modelVideo->findAll();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentVisitorForm()
    {
        $form = new Form;
        $form->addText('registered', 'Registrován:')->setDisabled();
        $form->addText('token', 'Token:')->setDisabled();
        $form->addText('invoice_company', 'Firma/jméno:')->setDisabled();
        $form->addText('invoice_email', 'E-mail:')->setDisabled();
        $form->addText('invoice_ic', 'IČ:')->setDisabled();
        $form->addText('invoice_dic', 'DIČ:')->setDisabled();
        $form->addText('invoice_street', 'Ulice:')->setDisabled();
        $form->addText('invoice_number', 'ČP:')->setDisabled();
        $form->addText('invoice_city', 'Město:')->setDisabled();
        $form->addText('invoice_zip', 'PSČ:')->setDisabled();
        $form->addSelect("paid", "Zaplaceno:", array( 0 => "Ne", 1 => "ANO"));

        $form->addSubmit('send', 'Upravit');
        $form->onSuccess[] = [$this, 'visitorFormSucceeded'];

        $form->setDefaults(array(
            'registered' => $this->visitor->registered,
            'token' => $this->visitor->token,
            'invoice_company' => $this->visitor->invoice_company,
            'invoice_email' => $this->visitor->invoice_email,
            'invoice_ic' => $this->visitor->invoice_ic,
            'invoice_dic' => $this->visitor->invoice_dic,
            'invoice_street' => $this->visitor->invoice_street,
            'invoice_number' => $this->visitor->invoice_number,
            'invoice_city' => $this->visitor->invoice_city,
            'invoice_zip' => $this->visitor->invoice_zip,
            'paid' => $this->visitor->paid,
        ));

        //if(($this->visitor->payments == "payment_bank_transfer") && ($this->visitor->paid==0)) {
        if(($this->visitor->paid==0)) {
            //pokud jeste neni zaplaceno, muzu zmenit
            $form["paid"]->setDisabled(FALSE);
        } else {
            $form["paid"]->setDisabled();
        }

        return $form;
    }

    public function visitorFormSucceeded(Form $form, $values)
    {
        //if(($this->visitor->payments == "payment_bank_transfer") && ($this->visitor->paid==0) && ($values["paid"]==1)) {
        if(($this->visitor->paid==0) && ($values["paid"]==1)) {
            //nastav uzivateli ze ma zaplaceno
            $this->visitor->update( array( "paid" => 1, "paid_date" => new Nette\Utils\DateTime() ) );

            $this->serviceVisitor->emitPaidEvent($this->visitor);//vytvor fakturu a posli ji emailem uzivateli

            $this->flashMessage("Uživateli jsem nastavil že má zaplaceno a nechal mu vygenerovat fakturu na idoklad.cz", "alert-success");
        }
        $this->redirect("Dashboard:users");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentVideoForm()
    {
        $form = new Form;
        $form->addText('registered', 'Registrován:')->setDisabled();
        $form->addText('token', 'Token:')->setDisabled();
        $form->addText('invoice_company', 'Firma/jméno:')->setDisabled();
        $form->addText('invoice_email', 'E-mail:')->setDisabled();
        $form->addText('invoice_ic', 'IČ:')->setDisabled();
        $form->addText('invoice_dic', 'DIČ:')->setDisabled();
        $form->addText('invoice_street', 'Ulice:')->setDisabled();
        $form->addText('invoice_number', 'ČP:')->setDisabled();
        $form->addText('invoice_city', 'Město:')->setDisabled();
        $form->addText('invoice_zip', 'PSČ:')->setDisabled();
        $form->addSelect("paid", "Zaplaceno:", array( 0 => "Ne", 1 => "ANO"));

        $form->addSubmit('send', 'Upravit');
        $form->onSuccess[] = [$this, 'videoFormSucceeded'];

        $form->setDefaults(array(
            'registered' => $this->video->registered,
            'token' => $this->video->token,
            'invoice_company' => $this->video->invoice_company,
            'invoice_email' => $this->video->invoice_email,
            'invoice_ic' => $this->video->invoice_ic,
            'invoice_dic' => $this->video->invoice_dic,
            'invoice_street' => $this->video->invoice_street,
            'invoice_number' => $this->video->invoice_number,
            'invoice_city' => $this->video->invoice_city,
            'invoice_zip' => $this->video->invoice_zip,
            'paid' => $this->video->paid,
        ));

        //if(($this->video->payments == "payment_bank_transfer") && ($this->video->paid==0)) {
        if(($this->video->paid==0)) {
            //pokud jeste neni zaplaceno, muzu zmenit
            $form["paid"]->setDisabled(FALSE);
        } else {
            $form["paid"]->setDisabled();
        }

        return $form;
    }

    public function videoFormSucceeded(Form $form, $values)
    {
        if(($this->video->paid==0) && ($values["paid"]==1)) {
            //nastav uzivateli ze ma zaplaceno
            $this->video->update( array( "paid" => 1, "paid_date" => new Nette\Utils\DateTime() ) );

            $this->invoiceCreator->createInvoiceVideo($this->video);//vytvor fakturu a posli ji emailem uzivateli
            //$this->serviceVisitor->emitPaidEvent($this->video);//vytvor fakturu a posli ji emailem uzivateli

            $this->flashMessage("Video uživateli jsem nastavil že má zaplaceno a nechal mu vygenerovat fakturu na idoklad.cz", "alert-success");
        }
        $this->redirect("Dashboard:videos");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentPartnersForm()
    {
        $form = new Form;
        $form->addTextArea('partners', '')
            ->setAttribute('cols', 100)
            ->setAttribute('rows', 50);

        $form->addSubmit('send', 'Upravit');
        $form->onSuccess[] = [$this, 'partnersFormSucceeded'];

        $form->setDefaults(array(
            'partners' => $this->setting->partners,
        ));

        return $form;
    }

    public function partnersFormSucceeded(Form $form, $values)
    {
        $this->setting->update(array("partners" => $values["partners"]));

        $this->flashMessage("Upraveno", "alert-success");
        $this->redirect("this");
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function speakerForm()
    {
        $form = new Form;
        $form->addText('name', 'Jmeno');
        $form->addText('photo', 'Fotka');
        $form->addText('expert', 'Expert');
        $form->addTextArea('description', 'Popis')
            ->setAttribute('cols', 100)
            ->setAttribute('rows', 10);

        $form->addText('lecture_name', 'Název přednášky');

        $form->addTextArea('lecture_annotation', 'Anotace přednášky')
            ->setAttribute('cols', 100)
            ->setAttribute('rows', 10);

        $form->addSelect('speaker_type_id', 'Velky recnik', $this->modelSpeakerType->getFP() );

        $form->addText('sort', 'Razeni');

        $form->addSelect("date", "Datum", array( "01.04.2016" => "pátek 01.04.2016", "02.04.2016" => "sobota 02.04.2016") )
            ->setPrompt('Žádná datum');

        $form->addText('time', "Čas začátku:")
            ->setAttribute( 'placeholder', 'Čas začátku' );

        return $form;
    }

    protected function createComponentSpeakerAddForm()
    {
        $form = $this->speakerForm();

        $form->addSubmit('send', 'Vytvořit');
        $form->onSuccess[] = [$this, 'speakerAddFormSucceeded'];

        return $form;
    }

    public function speakerAddFormSucceeded(Form $form, $values)
    {
        if( $values["date"] != "" ) {
            $values["date"] = new Nette\Utils\DateTime($values["date"]);
        } else {
            $values["date"] = NULL;
        }

        if( $values["time"] != "" ) {
            $values["time"] = date("H:i", strtotime($values["time"]));
        } else {
            $values["time"] = NULL;
        }

        $this->modelSpeaker->insert($values);

        $this->flashMessage("Speaker přidan", "alert-success");
        $this->redirect("Dashboard:speakers");
    }

    protected function createComponentSpeakerEditForm()
    {
        $form = $this->speakerForm();

        $form->addSubmit('send', 'Upravit');
        $form->onSuccess[] = [$this, 'speakerEditFormSucceeded'];

        $defaults = array(
            'name' => $this->speaker->name,
            'photo' => $this->speaker->photo,
            'expert' => $this->speaker->expert,
            'description' => $this->speaker->description,
            'lecture_name' => $this->speaker->lecture_name,
            'lecture_annotation' => $this->speaker->lecture_annotation,
            'speaker_type_id' => $this->speaker->speaker_type_id,
            'sort' => $this->speaker->sort
        );
        if ( $this->speaker->date != NULL ) {
            $defaults['date'] = $this->speaker->date->format('d.m.Y');
        }

        if ( $this->speaker->time != NULL ) {
            $defaults['time'] = $this->speaker->time->format('%H:%I');
        }

        $form->setDefaults($defaults);

        return $form;
    }

    public function speakerEditFormSucceeded(Form $form, $values)
    {
        if( $values["date"] != "" ) {
            $values["date"] = new Nette\Utils\DateTime($values["date"]);
        } else {
            $values["date"] = NULL;
        }

        if( $values["time"] != "" ) {
            $values["time"] = date("H:i", strtotime($values["time"]));
        } else {
            $values["time"] = NULL;
        }

        $this->speaker->update($values);

        $this->flashMessage("Speaker upraven", "alert-success");
        $this->redirect("Dashboard:speakers");
    }
}
