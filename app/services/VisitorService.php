<?php

namespace Services;

use Nette;

class VisitorService extends Nette\Object
{
    /** @var array of function(Backer $backer) */
    public $onPaid = [];


    public function emitPaidEvent(Nette\Database\Table\ActiveRow $visitorRow)
    {
        $this->onPaid($visitorRow);
    }

}