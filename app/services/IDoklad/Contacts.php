<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services\IDoklad;


class Contacts
{
	/** @var Client */
	private $client;

	/**
	 * Contacts constructor.
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		$this->client = $client;
	}


	public function create($contactData)
	{
		return $this->client->post('Contacts', [], $contactData);
	}
}
