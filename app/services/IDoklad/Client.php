<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services\IDoklad;


use Kdyby;
use Kdyby\Curl\CurlSender;
use Kdyby\Curl\Request;
use Kdyby\CurlCaBundle\CertificateHelper;


class Client
{
	/** @var string */
	private $endpoint = 'https://app.idoklad.cz/developer/api/';

	/** @var string */
	private $secureToken;

	/** @var string */
	private $appName;

	/** @var string */
	private $appVersion;

	/** @var CurlSender */
	private $curlSender;

	/**
	 * Client constructor.
	 * @param string $secureToken
	 * @param $appName
	 * @param $appVersion
	 */
	public function __construct($secureToken, $appName = NULL, $appVersion = NULL)
	{
		$this->secureToken = $secureToken;
		$this->appName = $appName;
		$this->appVersion = $appVersion;

		$this->curlSender = new CurlSender();
		$this->curlSender->setTrustedCertificate(CertificateHelper::getCaInfoFile());
	}

	/**
	 * @return string
	 */
	public function getEndpoint()
	{
		return $this->endpoint;
	}

	/**
	 * @param string $endpoint
	 */
	public function setEndpoint($endpoint)
	{
		$this->endpoint = $endpoint;
	}

	/**
	 * @return string
	 */
	public function getSecureToken()
	{
		return $this->secureToken;
	}

	/**
	 * @param string $secureToken
	 */
	public function setSecureToken($secureToken)
	{
		$this->secureToken = $secureToken;
	}

	/**
	 * @return mixed
	 */
	public function getAppName()
	{
		return $this->appName;
	}

	/**
	 * @param mixed $appName
	 */
	public function setAppName($appName)
	{
		$this->appName = $appName;
	}

	/**
	 * @return mixed
	 */
	public function getAppVersion()
	{
		return $this->appVersion;
	}

	/**
	 * @param mixed $appVersion
	 */
	public function setAppVersion($appVersion)
	{
		$this->appVersion = $appVersion;
	}



	public function get($uri, array $queryParams = [])
	{
		$req = $this->createRequest(Request::GET, $uri, $queryParams);
		return $this->performRequest($req);
	}

	public function post($uri, array $queryParams = [], $postBody = NULL)
	{
		$req = $this->createRequest(Request::POST, $uri, $queryParams, $postBody);
		return $this->performRequest($req);
	}

	public function put($uri, array $queryParams = [], $postBody = NULL)
	{
		$req = $this->createRequest(Request::PUT, $uri, $queryParams, $postBody);
		return $this->performRequest($req);
	}


	private function createRequest($method, $uri, array $queryParams = [], $postBody = NULL)
	{
		$url = $this->createUrl($uri, $queryParams);
		$request = new Request($url);
		$request->setMethod($method);
		$request->headers['Accept'] = 'application/json';
		if ($this->secureToken) {
			$request->headers['SecureToken'] = $this->secureToken;
		}
		if ($postBody !== NULL) {
			$request->headers['Content-Type'] = 'application/json';
			$request->setPost(json_encode($postBody));
		} else {
			$request->headers['Content-Length'] = 0;
		}
		if ($this->appName) {
			$request->headers['X-App'] = $this->appName;
		}
		if ($this->appVersion) {
			$request->headers['X-App-Version'] = $this->appVersion;
		}
		$request->setSender($this->curlSender);
        $request->setCertificationVerify(FALSE);
		return $request;
	}

	private function createUrl($uri, array $queryParams = [])
	{
		$uri = $this->endpoint . $uri;
		if (count($queryParams) > 0) {
			$uri .= '?' . http_build_query($queryParams);
		}
		return $uri;
	}

	private function performRequest(Request $request)
	{
        $resp = $request->send();
        $body = $resp->getResponse();
		return json_decode($body);
	}
}
