<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services\IDoklad;


class IssuedInvoices
{
	/** @var Client */
	private $client;

	/**
	 * IssuedInvoices constructor.
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		$this->client = $client;
	}



	public function getDefaultInvoice()
	{
		return $this->client->get('IssuedInvoices/Default');
	}


	public function createInvoice(array $invoiceData)
	{
		return $this->client->post('IssuedInvoices', [], $invoiceData);
	}

	public function sendMailToPurchaser($invoiceId)
	{
		$invoiceId = intval($invoiceId);
		return $this->client->put("IssuedInvoices/{$invoiceId}/SendMailToPurchaser");
	}
}
