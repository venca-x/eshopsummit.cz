<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services\ComGate;


use Kdyby\Curl\CurlSender;
use Kdyby\Curl\Request;
use Kdyby\CurlCaBundle\CertificateHelper;
use Nette\Http;


class Connector
{
	/** @var string */
	private $endpoint = 'https://payments.agmo.eu/v1.0/';

	/** @var string */
	private $merchant;

	/** @var string */
	private $secret;

	/** @var bool */
	private $testing = TRUE;

	/** @var CurlSender */
	private $curlSender;

	/**
	 * Connector constructor.
	 * @param string $merchant
	 * @param string $secret
	 * @param bool $testing
	 */
	public function __construct($merchant, $secret, $testing = TRUE)
	{
		$this->merchant = $merchant;
		$this->secret = $secret;
		$this->testing = $testing;

		$this->curlSender = new CurlSender();
		$this->curlSender->setTrustedCertificate(CertificateHelper::getCaInfoFile());
	}

	public function parseState(Http\Request $request)
	{
		$body = $request->getPost();
		if (!isset($body['merchant']) || $body['merchant'] !== $this->merchant) {
			throw new ConnectorException('Missing or invalid merchant identifier.');
		}
		if (isset($body['test']) && $body['test'] !== ($this->testing ? 'true' : 'false')) {
			throw new ConnectorException('Invalid test environment flag.');
		}
		if (!isset($body['secret']) && $body['secret'] !== $this->secret) {
			throw new ConnectorException('Missing or invalid secret.');
		}
		return $body;
	}

	public function createPayment($refId, $email, $country, $label, $price, $currency = 'CZK', $productName = NULL, $method = 'ALL')
	{
		$data = [
			'refId' => $refId,
			'country' => $country,
			'price' => $price,
			'curr' => $currency,
			'label' => $label,
			'email' => $email,
			'method' => $method,
			'cat' => 'DIGITAL',
			'prepareOnly' => 'true'
		];
		if ($productName) {
			$data['name'] = $productName;
		}
		return $this->post('create', $data);
	}


	private function buildUrl($uri)
	{
		return $this->endpoint . $uri;
	}

	private function createRequest($method, $uri, array $body = [])
	{
		$body['merchant'] = $this->merchant;
		$body['secret'] = $this->secret;
		if ($this->testing) {
			$body['test'] = 'true';
		} else {
			$body['test'] = 'false';
		}

		$url = $this->buildUrl($uri);
		$request = new Request($url);
		$request->setSender($this->curlSender);
		$request->setMethod($method);
		$request->setPost($body);
		return $request;
	}

	private function post($uri, array $body = [])
	{
		$request = $this->createRequest(Request::POST, $uri, $body);
		return $this->performRequest($request);
	}

	private function performRequest(Request $request)
	{
		$resp = $request->send();
		$responseString = $resp->getResponse();
		parse_str($responseString, $responseArray);
		if ($responseArray['code'] != 0) {
			throw new ConnectorException($responseArray['message']);
		}
		return $responseArray;
	}
}
