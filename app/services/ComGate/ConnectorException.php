<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services\ComGate;


class ConnectorException extends \Exception
{

}
