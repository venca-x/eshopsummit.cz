<?php
/**
 * Copyright (c) 2012, 2014 Jan Smitka <jan@smitka.org>, Lynt services s.r.o.
 *
 * You are permitted to use, copy, modify, and distribute this software
 * only with permission of the original author.
 *
 * This software is distributed "AS IS", without any warranties and/or
 * additional conditions of any kind, either expressed or implied.
 *
 *
 * @author Jan Smitka <jan@smitka.org>
 * @copyright Copyright (c) 2014, 2015 Jan Smitka, Lynt services s.r.o.
 */

namespace Services;

use App\Presenters\VideoFormPresenter;
use Nette;
use Kdyby\Events\Subscriber;

class InvoiceCreator implements Subscriber
{
	/** @var IDoklad\Contacts */
	private $contacts;

	/** @var IDoklad\IssuedInvoices */
	private $issuedInvoices;

	/**
	 * InvoiceCreator constructor.
	 * @param IDoklad\Contacts $contacts
	 * @param IDoklad\IssuedInvoices $issuedInvoices
	 */
	public function __construct(IDoklad\Contacts $contacts, IDoklad\IssuedInvoices $issuedInvoices)
	{
		$this->contacts = $contacts;
		$this->issuedInvoices = $issuedInvoices;
	}

	/**
	 * Returns an array of events this subscriber wants to listen to.
	 *
	 * @return array
	 */
	public function getSubscribedEvents()
	{
		return [
			//'OnCon\Crowdfunding\EntityServices\BackerService::onPaid' => 'createInvoice'
			'Services\VisitorService::onPaid' => 'createInvoice'
		];
	}

    /**
     * Uzicatel ma zaplaceno, vygeneruj mu fakturu
     * https://app.idoklad.cz/Developer/Help/cs/Api/POST-api-IssuedInvoices
     * @param Nette\Database\Table\ActiveRow $visitorRow
     */
	public function createInvoice(Nette\Database\Table\ActiveRow $visitorRow)
	{
        //vytvor kontakt pro fakturacni udaje
		$contactData = $this->createContactData($visitorRow);
		$contact = $this->contacts->create($contactData);//POST Contacts

        //dump($contact);
		$invoice = $this->issuedInvoices->getDefaultInvoice();
        $invoice->PurchaserId = $contact->Id;//Id odběratele//required

        $invoice->Description = 'Faktura eshopsummit.cz';
		$invoice->DateOfPayment = $invoice->DateOfIssue;//DateOfIssue = Datum vytvoření dokladu

        if($visitorRow->payments == "payment_bank_transfer") {
            $invoice->PaymentOptionId = 1;//prevodem
        } else {
            $invoice->PaymentOptionId = 2;//kartou
        }

        $invoice->VariableSymbol = $visitorRow->variable_symbol;//kali chce abych daval variabilni symbol i na faktury platebni kartou

		$itemDefault = $invoice->IssuedInvoiceItems[0];//vzor polozky faktury vydane
		$invoice->IssuedInvoiceItems = [];//Polozky faktury vydane//required

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach($visitorRow->related('visitor_ticket.visitor_id') as $row) {
            $item = clone $itemDefault;
            $item->Name = $row->name . ", " . $row->ticket->name_invoice;
            $item->UnitPrice = $row->ticket->price;
            $item->PriceType = 0;//0 = WithVat
            $item->VatRateType = 2;//2 = Nulová sazba DPH
            $invoice->IssuedInvoiceItems[] = $item;//polozka faktury
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //pokud je to faktura se slevou, posilam naci jeste zapornou polozku, ktera signalizuje slevu
        if($visitorRow->discount_id != NULL) {
            //uzivatel ma slevu
            $item = clone $itemDefault;
            $item->Name = "Slevový poukaz " . $visitorRow->discount->name;
            $item->UnitPrice = (0 - $visitorRow->discount_price);
            $item->PriceType = 0;//0 = WithVat
            $item->VatRateType = 2;//2 = Nulová sazba DPH
            $invoice->IssuedInvoiceItems[] = $item;//polozka faktury
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$response = $this->issuedInvoices->createInvoice((array) $invoice);
		$id = $response->Id;
        $visitorRow->update( array( "invoice_number_idoklad" => $response->DocumentNumber ) );//cislo faktury si ulozim

		$this->issuedInvoices->sendMailToPurchaser($id);
	}

    /**
     * Uzicatel ma zaplaceno, vygeneruj mu fakturu
     * https://app.idoklad.cz/Developer/Help/cs/Api/POST-api-IssuedInvoices
     * @param Nette\Database\Table\ActiveRow $videoRow
     */
    public function createInvoiceVideo(Nette\Database\Table\ActiveRow $videoRow)
    {
        //vytvor kontakt pro fakturacni udaje
        $contactData = $this->createContactData($videoRow);
        $contact = $this->contacts->create($contactData);//POST Contacts

        //dump($contact);
        $invoice = $this->issuedInvoices->getDefaultInvoice();
        $invoice->PurchaserId = $contact->Id;//Id odběratele//required

        $invoice->Description = 'Faktura eshopsummit.cz - video';
        $invoice->DateOfPayment = $invoice->DateOfIssue;//DateOfIssue = Datum vytvoření dokladu

        if($videoRow->payments == "payment_bank_transfer") {
            $invoice->PaymentOptionId = 1;//prevodem
        } else {
            $invoice->PaymentOptionId = 2;//kartou
        }

        $invoice->VariableSymbol = $videoRow->variable_symbol;//kali chce abych daval variabilni symbol i na faktury platebni kartou

        $itemDefault = $invoice->IssuedInvoiceItems[0];//vzor polozky faktury vydane
        $invoice->IssuedInvoiceItems = [];//Polozky faktury vydane//required

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $item = clone $itemDefault;
        $item->Name = VideoFormPresenter::INVOICE_ITEM_TEXT;
        $item->UnitPrice = VideoFormPresenter::VIDEO_PRICE;
        $item->PriceType = 0;//0 = WithVat
        $item->VatRateType = 2;//2 = Nulová sazba DPH

        $invoice->IssuedInvoiceItems[] = $item;//polozka faktury

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $response = $this->issuedInvoices->createInvoice((array) $invoice);
        $id = $response->Id;
        $videoRow->update( array( "invoice_number_idoklad" => $response->DocumentNumber ) );//cislo faktury si ulozim

        $this->issuedInvoices->sendMailToPurchaser($id);
    }

    /**
     * Pro zakoupeni vstupenky i pro video
     * @param Nette\Database\Table\ActiveRow $visitorRow
     * @return array
     */
	private function createContactData(Nette\Database\Table\ActiveRow $visitorRow)
	{
		$data = [
			'Email' => $visitorRow->invoice_email
		];

        $data['CountryId'] = $visitorRow->country_id; //required //2 = CZ, 1 = SK
        $data['CompanyName'] = $visitorRow->invoice_company;//required

        //$data['Firstname'] = "Jmeno";
        //$data['Surname'] = "Prijmeni";
        if($visitorRow->invoice_ic) {
            $data['IdentificationNumber'] = $visitorRow->invoice_ic;//IC
        }

        if($visitorRow->invoice_dic) {
            if ($visitorRow->country_id == 2) {
                $data['VatIdentificationNumber'] = $visitorRow->invoice_dic;//DIC CZ
            } else {
                $data['VatIdentificationNumberSk'] = $visitorRow->invoice_dic;//DIC SK
            }
        }

        if($visitorRow->invoice_street) {
            $street = $visitorRow->invoice_street;
        } else {
            $street = $visitorRow->invoice_city;
        }
        $street = $street . " " . $visitorRow->invoice_number;//cislo popisne

        $data['Street'] = $street;
        $data['City'] = $visitorRow->invoice_city;
        $data['PostalCode'] = $visitorRow->invoice_zip;

		return $data;
	}
}
