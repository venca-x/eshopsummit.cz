<?php

namespace App\Presenters;

use Nette;
use App\Factory;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

class HomepagePresenter extends BasePresenter
{
    /** @var Factory\ContainerLocatorFactory */
    public $factoryContainerLocator;

	/** @var Model\Interest */
	public $modelInterest;

    /** @var Model\Lecture */
    public $modelLecture;

    /** @var Model\LecturerTip */
    public $modelLecturerTip;

    /** @var Model\Setting */
    public $modelSetting;

    /** @var Model\Speaker */
    public $modelSpeaker;

    /** @var Model\Ticket */
	public $modelTicket;

    /** @var bool Zobraz formular pro newsletter (pokud se uzivatel prida do newsletter, formular se skryje)*/
    protected $showInterestForm = TRUE;

    /** @var bool Tip na recnika byl odeslan. Zobrazim hlasku ze dekujeme*/
    protected $lecturerTipSuccessSubmit = FALSE;

	public function __construct(
                                 Factory\ContainerLocatorFactory $factoryContainerLocator,
								 Model\Interest $modelInterest,
                                 Model\Lecture $modelLecture,
                                 Model\LecturerTip $modelLecturerTip,
                                 Model\Setting $modelSetting,
                                 Model\Speaker $modelSpeaker,
								 Model\Ticket $modelTicket
	)
	{
        $this->factoryContainerLocator = $factoryContainerLocator;
		$this->modelInterest = $modelInterest;
        $this->modelLecture = $modelLecture;
        $this->modelLecturerTip = $modelLecturerTip;
        $this->modelSetting = $modelSetting;
        $this->modelSpeaker = $modelSpeaker;
		$this->modelTicket = $modelTicket;
	}

	public function renderDefault() {
		$this->template->setting = $this->modelSetting->getRow();

		$this->template->tickets = $this->modelTicket->getAllClassics();
		$this->template->ticketVIP = $this->modelTicket->getVIP();

		$this->template->showInterestForm = $this->showInterestForm;
		$this->template->lecturerTipSuccessSubmit = $this->lecturerTipSuccessSubmit;
        $this->template->normalTicketForNow = $this->modelTicket->getClassicTicketForNow();

        $this->template->bigSpeakers = $this->modelSpeaker->findBigSpeakers();
        $this->template->smallSpeakers = $this->modelSpeaker->findSmallSpeakers();

        $this->template->panelOperationSpeakers = $this->modelSpeaker->findPanelOperationSpeakers();
        $this->template->panelMarketingSpeakers = $this->modelSpeaker->findPanelMarketingSpeakers();
        $this->template->caseStudySpeakers = $this->modelSpeaker->findCaseStudySpeakers();
	}

    /**
     * Od	Do	Trvání
     * 10:00	10:10	0:10	Úvodní slovo
     * 10:10	10:45	0:35	1. řečník
     * 10:45	11:35	0:50	2. řečník
     * 11:35	12:00	0:25	Break 1
     * 12:00	12:35	0:35	3. řečník
     * 12:35	13:10	0:35	4. řečník
     * 13:10	14:25	1:15	Oběd
     * 14:25	15:15	0:50	Panelová diskuze - provoz e-shop
     * 15:15	15:50	0:35	5. řečník
     * 15:50	16:15	0:25	Break 2
     * 16:15	17:15	1:00	Blok případových studií
     * 17:15	17:50	0:35	6. řečník
     * 17:50	17:55	0:05	Závěr dne
     *
     * Druhy den
     *Od	Do	Trvání
     *9:15	9:20	0:05	Přivítání 2. dne
     *9:20	9:55	0:35	7. řečník
     *9:55	10:45	0:50	Panelová diskuze - marketing e-shopu
     *10:45	11:10	0:25	Break 1
     *11:10	11:45	0:35	8. řečník
     *11:45	12:20	0:35	9. řečník
     *12:20	12:55	0:35	10. řečník
     *12:55	14:10	1:15	Oběd
     *14:10	14:45	0:35	11. řečník
     *14:45	15:20	0:35	12. řečník
     *15:20	15:45	0:25	Break 2
     *15:45	16:20	0:35	13. řečník
     *16:20	16:55	0:35	14. řečník
     *16:55	17:00	0:05	Závěrečné poděkování
     *
     */
    public function renderProgram() {

        $programFridayArray = array();
        $dateFriday = new DateTime("01.04.2016");
        $programFridayArray[] = new Model\ProgramBluePoint("Úvodní slovo", "10:00");
        foreach( $this->modelSpeaker->getProgramTo($dateFriday, "00:00", "11:35") as $row){
            $programFridayArray[] = $row;
        }
        $programFridayArray[] = new Model\ProgramCoffee("Coffe break", "11:35");
        foreach( $this->modelSpeaker->getProgramTo($dateFriday, "11:35", "13:10") as $row){
            $programFridayArray[] = $row;
        }
        $programFridayArray[] = new Model\ProgramLunch("Oběd", "13:10");

        //Pátek - 14:25 až 15:15 - Panelová diskuze - provoz e-shopu
        $discussion = array( "time" => "14:25", "title" => "Panelová diskuze - provoz e-shopu", "text" => "", "id" => "panelova-diskuse-provoz-e-shopu");
        foreach($this->modelSpeaker->findPanelOperationSpeakers() as $speaker) {
            $discussion["text"] .= $speaker->name . "\n";
        }
        $programFridayArray[] = $discussion;
        foreach( $this->modelSpeaker->getProgramTo($dateFriday, "13:10", "15:50") as $row){
            $programFridayArray[] = $row;
        }
        $programFridayArray[] = new Model\ProgramCoffee("Coffe break", "15:50");

        //Pátek - 16:15 až 17:15 - Blok případových studií
        $study = array( "time" => "16:15", "title" => "Blok případových studií", "text" => "", "id" => "pripadova-studie");
        foreach($this->modelSpeaker->findCaseStudySpeakers() as $speaker) {
            $study["text"] .= $speaker->name . "\n";
        }
        $programFridayArray[] = $study;
        foreach( $this->modelSpeaker->getProgramTo($dateFriday, "15:50", "17:50") as $row){
            $programFridayArray[] = $row;
        }
        $programFridayArray[] = new Model\ProgramBluePoint("Závěr dne", "17:50");
        //$this->template->dayFriday = $programFridayArray;



        $programSaturdayArray = array();
        $dateSaturday = new DateTime("02.04.2016");
        $programSaturdayArray[] = new Model\ProgramBluePoint("Přivítání 2. dne", "9:15");
        foreach( $this->modelSpeaker->getProgramTo($dateSaturday, "00:00", "10:45") as $row){
            $programSaturdayArray[] = $row;
        }
        //Sobota - 9:55 až 10:45 - Panelová diskuze - marketing e-shopu
        $discussion = array( "time" => "9:55", "title" => "Panelová diskuze - marketing e-shopu", "text" => "", "id" => "panelova-diskuse-marketing-e-shopu");
        foreach($this->modelSpeaker->findPanelMarketingSpeakers() as $speaker) {
            $discussion["text"] .= $speaker->name . "\n";
        }
        $programSaturdayArray[] = $discussion;
        $programSaturdayArray[] = new Model\ProgramCoffee("Coffe break", "10:45");
        foreach( $this->modelSpeaker->getProgramTo($dateSaturday, "10:45", "12:55") as $row){
            $programSaturdayArray[] = $row;
        }
        $programSaturdayArray[] = new Model\ProgramLunch("Oběd", "12:55");
        foreach( $this->modelSpeaker->getProgramTo($dateSaturday, "12:55", "15:20") as $row){
            $programSaturdayArray[] = $row;
        }
        $programSaturdayArray[] = new Model\ProgramCoffee("Coffe break", "15:20");
        foreach( $this->modelSpeaker->getProgramTo($dateSaturday, "15:20", "16:55") as $row){
            $programSaturdayArray[] = $row;
        }
        $programSaturdayArray[] = new Model\ProgramBluePoint("Závěrečné poděkování", "16:55");
        //$this->template->daySaturday = $programSaturdayArray;

        $programArray = array();
        $programArray["Pátek " . $dateFriday->format("j.n.Y")] = $programFridayArray;
        $programArray["Sobota " . $dateSaturday->format("j.n.Y")] = $programSaturdayArray;
        $this->template->program = $programArray;
    }

    /**
     * Prvni formular pro newsletter
     * @return Form
     */
    protected function createComponentInterestForm()
    {
        $form = $this->interestForm();
        $form->onSuccess[] = $this->interestFormSucceeded;
        return $form;
    }

    /**
     * Druhy formular pro newsletter
     * @return Form
     */
    protected function createComponentInterestForm2()
    {
        $form = $this->interestForm();
        $form->onSuccess[] = $this->interestFormSucceeded2;
        return $form;
    }

    protected function interestForm() {
        $form = new Form;
        $form->getElementPrototype()->class( 'ajax' );

        $form->addText('email', 'E-mail:')
            ->setType('email')
            ->setAttribute('placeholder', 'Zde nám zanechte svůj e-mail')
            ->addRule(Form::EMAIL, 'Zadaný e-mail nemá správný formát');

        $form->addSubmit('send', 'Odeslat');
        return $form;
    }

    public function interestFormSucceeded(Form $form, $values)
    {
        $this->addUserToInterest($form, $values);
        if ($this->isAjax()) {
            $this->redrawControl( 'newsletter' );//znovu vykresli snippet workshop
        }
        else
        {
            $this->redirect('this', array("interest" => "send"));
        }
    }

    public function interestFormSucceeded2(Form $form, $values)
    {
        $this->addUserToInterest($form, $values);
        if ($this->isAjax()) {
            $this->redrawControl( 'newsletter2' );//znovu vykresli snippet workshop
        }
        else
        {
            $this->redirect('this', array("interest" => "send"));
        }
    }

    protected function addUserToInterest(Form $form, $values)
    {
        $row = $this->modelInterest->findByEmail($values["email"]);
        if ($row != FALSE) {
            //email je jiz zaregistrovan
            $form->addError('Jejda, už Váš již e-mail máme... Víme o Vás a až bude mít důležité infromace, napíšeme Vám to!');
            return;
        } else {
            $values["date"] = new DateTime();
            $values["ip"] = $this->factoryContainerLocator->getRemoteAddress();

            $this->modelInterest->insert($values);
            $this->showInterestForm = FALSE;
        }
    }

    /**
     * Tip na prednasejiciho
     * @return Form
     */
    protected function createComponentLecturerTipForm()
    {
        $form = new Form;
        $form->getElementPrototype()->class( 'ajax' );

        $form->addText('lecturer_name', 'Jméno navrhovaného řečníka:')->setRequired("Zadejte jméno navrhovaného řečníka");
        $form->addText('author', 'Tvé jméno:')->setRequired("Zadejte Vaše jméno");

        $form->addText('author_email', 'Tvůj e-mail:')
            ->setType('email');

        $form->addSubmit('send', 'Navrhnout řečníka');

        $form->onSuccess[] = $this->lectureTipFormSucceeded;
        return $form;
    }

    public function lectureTipFormSucceeded(Form $form, $values)
    {
        $values["date"] = new DateTime();
        $this->modelLecturerTip->insert($values);
        $this->lecturerTipSuccessSubmit = TRUE;

        if ($this->isAjax()) {

            $form->components["lecturer_name"]->setValue("");
            $form->components["author"]->setValue("");
            $form->components["author_email"]->setValue("");

            $this->redrawControl( 'lecturerFormTip' );
        }
        else
        {
            $this->redirect('this', array("lecturer-tip" => "send"));
        }
    }



    /**
     * Tip na prednasejiciho
     * @return Form
     */
    protected function createComponentStudiesAddForm()
    {
        $form = new Form;
        //$form->getElementPrototype()->class( 'ajax' );

        $type = array(
            'studie' => 'Mám případovou studii',
            'story' => 'Mám osobní příběh',
        );
        $form->addRadioList('type', '', $type)->setRequired("Vyberte typ");

        $form->addText('name', 'Jméno a příjmení:')->setRequired("Zadejte jméno a příjmení");

        $form->addText('email', 'E-mail:')
            ->setType('email')
            ->setRequired("Zadejte e-mail");

        $form->addText('phone', 'Telefon:')->setRequired("Zadejte Vaše telefonní číslo");
        $form->addTextArea('description', 'Stručný popis:')->setRequired("Zadejte stručný popis");

        $form->addUpload('upload', 'Připojit přílohu:')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 64 kB.', 5 * 1024 * 1024 /* v bytech */);//5MB

        $form->addSubmit('send', 'Odeslat');

        $getArray = $this->factoryContainerLocator->getHttpQuery();
        $type = "studie";
        if(isset($getArray["type"])) {
            if($getArray["type"] == "story") {
                $type = "story";
            }
        }
        $form->setDefaults(array(
            'type' => $type
        ));

        $form->onSuccess[] = $this->studiesAddFormSucceeded;
        return $form;
    }

    public function studiesAddFormSucceeded(Form $form, $values)
    {
        $values["date"] = new DateTime();

        $upload = $values["upload"];
        unset( $values["upload"] );

        $lectureRow = $this->modelLecture->insert($values);

        if ( $upload->isOk() )
        {
            $fileName = UPLOAD_FOLDER . $lectureRow->id . "-" . time() . "." . pathinfo( $upload->getSanitizedName(), PATHINFO_EXTENSION );
            $upload->move( WWW_DIR . "" . $fileName );
            $lectureRow->update( array( "attachement" => $fileName ) );
        }

        $this->flashMessage( 'Zadaná data byla odeslána. Děkujeme', "alert-success" );
        $this->redirect('this', array("lecture-add" => "ok"));
    }

}
