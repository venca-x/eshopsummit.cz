<?php

namespace App\Presenters;

use App\Model\PaymentTransaction;
use App\Model\VideoPaymentTransaction;
use Nette\Utils\DateTime;
use Services\ComGate;
use Tracy\Debugger;
use App\Model;

class ContributionPresenter extends BasePresenter
{

    /** @var ComGate\Connector */
    private $paymentConnector;

    /** @var Model\PaymentTransaction */
    protected $modelPaymentTransaction;

    /** @var Model\Video */
    protected $modelVideo;

    /** @var Model\VideoPaymentTransaction */
    protected $modelVideoPaymentTransaction;

    /** @var Model\Visitor */
    protected $modelVisitor;

    /**
     * ContributionPresenter constructor.
     * @param ComGate\Connector $connector
     */
    public function __construct(ComGate\Connector $connector,
                                Model\PaymentTransaction $modelPaymentTransaction,
                                Model\Video $modelVideo,
                                Model\VideoPaymentTransaction $modelVideoPaymentTransaction,
                                Model\Visitor $modelVisitor)
    {
        parent::__construct();
        $this->paymentConnector = $connector;
        $this->modelPaymentTransaction = $modelPaymentTransaction;
        $this->modelVideo = $modelVideo;
        $this->modelVideoPaymentTransaction = $modelVideoPaymentTransaction;
        $this->modelVisitor = $modelVisitor;
    }

    /**
     * Zaplaceni kartou
     * @param $token
     */
    public function actionPayment($token)
    {
        $visitorRow = $this->modelVisitor->findByToken($token);
        $videoRow = $this->modelVideo->findByToken($token);
        if($visitorRow != FALSE) {
            if ($visitorRow->paid == 1) {
                $this->redirect('state', ['token' => $visitorRow->token]);
            }

            $notCancelledTransactions = $visitorRow->related('payment_transaction.visitor_id')->where("payment_transaction.state", PaymentTransaction::PENDING);

            if ($notCancelledTransactions->count() > 0) {
                foreach ($notCancelledTransactions as $paymentTransactionRow) {
                    $this->redirectUrl($paymentTransactionRow->redirect_url);//presmeruj na URL ktere jiz existuje
                }
            }

            $productNames = array();//[ "vstupenka1" => "vstupenka2", "vstupenka3" => "vstupenka4" ];
            foreach ($visitorRow->related('visitor_ticket.visitor_id') as $row) {
                $productNames[] = $row->name . " - " . $row->ticket->name_invoice;
            }

            try {
                $resp = $this->paymentConnector->createPayment($token, $visitorRow->invoice_email,
                    strtoupper($visitorRow->country->abbreviation), 'Vstupenky na eshopsummit.cz', $visitorRow->price * 100,
                    'CZK', implode(', ', $productNames));

                $paymentTransactionArray = array();
                $paymentTransactionArray["date"] = new DateTime();
                $paymentTransactionArray["visitor_id"] = $visitorRow->id;
                $paymentTransactionArray["transaction_id"] = $resp['transId'];
                $paymentTransactionArray["redirect_url"] = $resp['redirect'];
                $paymentTransactionArray["state"] = PaymentTransaction::PENDING;

                $this->modelPaymentTransaction->insert($paymentTransactionArray);

                $this->redirectUrl($resp['redirect']);//redirect to payment gateway

            } catch (ComGate\ConnectorException $e) {
                Debugger::log($e, Debugger::EXCEPTION);
                $this->setView('paymentError');
            }
        } else if($videoRow != FALSE) {
            if ($videoRow->paid == 1) {
                $this->redirect('stateVideo', ['token' => $videoRow->token]);
            }

            $notCancelledTransactions = $videoRow->related('video_payment_transaction.video_id')->where("video_payment_transaction.state", PaymentTransaction::PENDING);

            if ($notCancelledTransactions->count() > 0) {
                foreach ($notCancelledTransactions as $paymentTransactionRow) {
                    $this->redirectUrl($paymentTransactionRow->redirect_url);//presmeruj na URL ktere jiz existuje
                }
            }

            try {
                $resp = $this->paymentConnector->createPayment($token, $videoRow->invoice_email,
                    strtoupper($videoRow->country->abbreviation), 'Vstupenky na eshopsummit.cz - video', $videoRow->price * 100,
                    'CZK', VideoFormPresenter::INVOICE_ITEM_TEXT);

                $paymentTransactionArray = array();
                $paymentTransactionArray["date"] = new DateTime();
                $paymentTransactionArray["video_id"] = $videoRow->id;
                $paymentTransactionArray["transaction_id"] = $resp['transId'];
                $paymentTransactionArray["redirect_url"] = $resp['redirect'];
                $paymentTransactionArray["state"] = VideoPaymentTransaction::PENDING;
                $this->modelVideoPaymentTransaction->insert($paymentTransactionArray);

                $this->redirectUrl($resp['redirect']);//redirect to payment gateway

            } catch (ComGate\ConnectorException $e) {
                Debugger::log($e, Debugger::EXCEPTION);
                $this->setView('paymentErrorVideo');
            }

        } else {
            Debugger::log("Token " . $token . " nenalezen v tabulce visitor ani video", Debugger::EXCEPTION);
            $this->setView('paymentError');
        }
    }


    /**
     * Semhle posila platebni brana? mi z toho uz hrabne....
     * @param $token
     * @param null $state
     * @throws \Nette\Application\BadRequestException
     */
    public function renderState($token, $state = NULL)
    {
        //echo "ContributionPresenter:state - token: " . $token . ", state: " . $state;

        $visitorRow = $this->modelVisitor->findByToken($token);
        $videoRow = $this->modelVideo->findByToken($token);

        if($visitorRow != FALSE) {
            $this->template->visitorRow = $visitorRow;
            $this->template->rawState = $state;
        } else if($videoRow != FALSE) {
            $this->template->videoRow = $videoRow;
            $this->template->rawState = $state;

            //zobraz jinou sablonu
            $this->setView('stateVideo');
        } else {
            $this->raise404();
        }


    }

    public function renderStateVideo($token, $state = NULL)
    {
        //echo "ContributionPresenter:state - token: " . $token . ", state: " . $state;
        $videoRow = $this->modelVideo->findByToken($token);
        if($videoRow == FALSE || $videoRow->token !== $token) {
            $this->raise404();
        }

        $this->template->videoRow = $videoRow;
        $this->template->rawState = $state;
    }
}