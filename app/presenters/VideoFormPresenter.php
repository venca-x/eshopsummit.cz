<?php

namespace App\Presenters;

use DateInterval;
use Nette;
use App\Factory;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Services\ComGate;
use Latte;
use Tracy\Debugger;


class VideoFormPresenter extends BasePresenter
{
    const INVOICE_ITEM_TEXT = "Videa z akce E-shop summit 2016 – předprodej";
    const VIDEO_PRICE = 490;//kc

    const PAYMENT_CARD = "payment_card";
    const PAYMENT_BANK_TRANSFER = "payment_bank_transfer";

    /** @var Factory\ContainerLocatorFactory */
    public $factoryContainerLocator;

    /** @var Factory\EmailFactory */
    private $factoryEmail;

    /** @var Model\Country */
    protected $modelCountry;

    /** @var Model\Video */
    protected $modelVideo;

    /** @var ComGate\Connector */
    private $paymentConnector;

    public function __construct(
        Factory\ContainerLocatorFactory $factoryContainerLocator,
        Factory\EmailFactory $factoryEmail,
        Model\Country $modelCountry,
        Model\Video $modelVideo,
        ComGate\Connector $connector
    )
    {
        $this->factoryContainerLocator = $factoryContainerLocator;
        $this->factoryEmail = $factoryEmail;
        $this->modelCountry = $modelCountry;
        $this->modelVideo = $modelVideo;
        $this->paymentConnector = $connector;
    }

    public function actionDefault()
    {
        $this->redirect("Homepage:");
    }

    public function renderDefault()
    {
        $this->template->videoPrice = SELF::VIDEO_PRICE;
    }
/*
    public function renderFake()
    {
        $visitorRow = $this->modelVisitor->findOneBy(array("payments" => "payment_bank_transfer"));
        $this->redirect("Form:bankTransfer", array('token' => $visitorRow->token));
    }*/


    /**
     * Zobrazeni dekovaci stranky pri platbe kartou
     * @param $token
     */
    public function renderBankTransfer($token) {

        $videoRow = $this->modelVideo->findByToken($token);

        if($videoRow != FALSE && $videoRow->payments == "payment_bank_transfer") {
            $date = $videoRow->registered;
            //$date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti za 0 dni

            $this->template->maturityDate = $maturityDate;
            $this->template->videoRow = $videoRow;
        } else {
            Debugger::log("Video: Pltaba kartou, token " . $token . " nenalezen v tabulce video", Debugger::EXCEPTION);
            $this->setView('paymentError');
        }
        $this->setView('bankTransfer');
    }

	protected function createComponentVideoForm()
	{
        $form = new Nette\Application\UI\Form;

        $renderer = $form->getRenderer();
        $renderer->wrappers['label']['requiredsuffix'] = "*";

        //////////////////////////////// start fakturacni udaje

        $invoice = $form->addContainer('invoice');
        $invoice->addText('company', 'Jméno/firma:')->setRequired("Zadejte Vaše jméno / firmu");
        $invoice->addText('email', 'Email:')->setType('email')->addRule(Form::EMAIL, 'Zadaný e-mail nemá správný formát')->setRequired("Zadejte e-mail");
        $invoice->addText('ic', 'IČ:');
        $invoice->addText('street', 'Ulice:');
        $invoice->addText('city', 'Město:')->setRequired("Zadejte město");
        $invoice->addText('dic', 'DIČ:');
        $invoice->addText('number', 'ČP:')->setRequired("Zadejte ČP");
        $invoice->addText('zip', 'PSČ:')->setRequired("Zadejte PSČ");

        $invoice->addSelect('country', 'Stát:', $this->modelCountry->getFP());
        //////////////////////////////// end fakturacni udaje

        $payments = array(
            self::PAYMENT_CARD => Html::el()->setHtml("Platební karta <img src='/images/pay_card.png' alt='Podporované platební karty'>"),
            self::PAYMENT_BANK_TRANSFER => "Bankovní převod",
        );
        $form->addRadioList('payments', '', $payments)->setDefaultValue(self::PAYMENT_CARD);

        $form->addSubmit('send', 'Objednat');


        $form->onSuccess[] = $this->videoFormSubmit;

        return $form;
	}

    public function videoFormSubmit(Form $form, $values)
    {

        //dump($values);exit;

        $visitorArray = array();
        $visitorArray["registered"] = new Nette\Utils\DateTime();
        $visitorArray["token"] = Random::generate(32);
        $visitorArray["invoice_company"] = $values["invoice"]["company"];
        $visitorArray["invoice_email"] = $values["invoice"]["email"];
        $visitorArray["invoice_ic"] = $values["invoice"]["ic"];
        $visitorArray["invoice_dic"] = $values["invoice"]["dic"];
        $visitorArray["invoice_street"] = $values["invoice"]["street"];
        $visitorArray["invoice_number"] = $values["invoice"]["number"];
        $visitorArray["invoice_city"] = $values["invoice"]["city"];
        $visitorArray["invoice_zip"] = $values["invoice"]["zip"];
        $visitorArray["country_id"] = $values["invoice"]["country"];

        $visitorArray["price"] = VideoFormPresenter::VIDEO_PRICE;//cena
        $visitorArray["payments"] = $values["payments"];//zpusob platby


        $videoRow = $this->modelVideo->insert($visitorArray);

        if($videoRow->payments == "payment_bank_transfer") {

            //platba bankovnim prevodem, nastav variabilni symbol
            $videoRow->update(array( "variable_symbol" => Factory\SettingsFactory::getUserVariableSymbolVideo($videoRow->id)));

            $date = new DateTime();
            //$date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti je dnes

            $latte = new Latte\Engine;
            $params = array(
                'videoRow' => $videoRow,
                'maturityDate' => $maturityDate
            );

            try {
                $this->factoryEmail->sendEmail(NULL, $videoRow->invoice_email, "Nákup videí z E-shop summit 2016 | Potvrzení objednávky", $latte->renderToString( "./../app/presenters/templates/email/bankTrasnferVideo.latte", $params));
            } catch (Nette\Mail\SmtpException $e) {
                $this->flashMessage( "Nepodařilo se Vám odeslat email s informacemi o platbě (".$e->getMessage().")", "alert-danger" );
                Debugger::log("Uživateli id: ".$videoRow->id.", token: ".$videoRow->token.", email: ".$videoRow->invoice_email." se nepodařilo odeslat email při platbě prevodem: " . $e->getMessage(), Debugger::EXCEPTION);
            }
            $this->redirect("VideoForm:bankTransfer", array('token' => $videoRow->token));

        } else {

            //platba kartou, nastav variabilni symbol - Kali chce variabilni symbol na fakture i pri platbe kartou
            $videoRow->update(array( "variable_symbol" => Factory\SettingsFactory::getUserVariableSymbolVideo($videoRow->id)));

            $date = new DateTime();
            //$date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti je dnes

            $latte = new Latte\Engine;
            $params = array(
                'videoRow' => $videoRow,
                'maturityDate' => $maturityDate,
                'payUrl' => $this->link("//Contribution:payment", $videoRow->token)
            );

            try {
                $this->factoryEmail->sendEmail(NULL, $videoRow->invoice_email, "Nákup videí z E-shop summit 2016 | Potvrzení objednávky", $latte->renderToString("./../app/presenters/templates/email/cardTrasnferVideo.latte", $params));
            } catch (Nette\Mail\SmtpException $e) {
                $this->flashMessage( "Nepodařilo se Vám odeslat email s informacemi o platbě (".$e->getMessage().")", "alert-danger" );
                Debugger::log("Uživateli id: ".$videoRow->id.", token: ".$videoRow->token.", email: ".$videoRow->invoice_email." se nepodařilo odeslat email při platbě kartou: " . $e->getMessage(), Debugger::EXCEPTION);
            }

            $this->redirect("Contribution:payment", array('token' => $videoRow->token));
        }
    }

}
