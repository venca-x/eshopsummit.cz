<?php

namespace App\Presenters;

use DateInterval;
use Nette;
use App\Factory;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Services\ComGate;
use Latte;
use Tracy\Debugger;


class FormPresenter extends BasePresenter
{
    const CLASSIC = "classic";
    const VIP = "vip";

    const PAYMENT_CARD = "payment_card";
    const PAYMENT_BANK_TRANSFER = "payment_bank_transfer";

    /** @var Factory\ContainerLocatorFactory */
    public $factoryContainerLocator;

    /** @var Factory\EmailFactory */
    private $factoryEmail;

    /** @var Model\Country */
    protected $modelCountry;

    /** @var Model\Discount */
    protected $modelDiscount;

    /** @var Model\Interest */
    protected $modelInterest;

    /** @var Model\Ticket */
    protected $modelTicket;

    /** @var Model\Visitor */
    protected $modelVisitor;

    /** @var Model\VisitorTicket */
    protected $modelVisitorTicket;

    /** @var ComGate\Connector */
    private $paymentConnector;

    /** @var Nette\Database\Table\ActiveRow */
    protected $normalTicketForNow;

    /** @var Nette\Database\Table\ActiveRow */
    protected $vipTicketForNow;

    /** @var Nette\Database\Table\ActiveRow */
    private $discountRow = null;

    /** @var */
    protected $totalPriceTicket;

    public function __construct(
        Factory\ContainerLocatorFactory $factoryContainerLocator,
        Factory\EmailFactory $factoryEmail,
        Model\Country $modelCountry,
        Model\Discount $modelDiscount,
        Model\Interest $modelInterest,
        Model\Ticket $modelTicket,
        Model\Visitor $modelVisitor,
        Model\VisitorTicket $modelVisitorTicket,
        ComGate\Connector $connector
    )
    {
        $this->factoryContainerLocator = $factoryContainerLocator;
        $this->factoryEmail = $factoryEmail;
        $this->modelCountry = $modelCountry;
        $this->modelDiscount = $modelDiscount;
        $this->modelInterest = $modelInterest;
        $this->modelTicket = $modelTicket;
        $this->modelVisitor = $modelVisitor;
        $this->modelVisitorTicket = $modelVisitorTicket;
        $this->paymentConnector = $connector;
    }

    public function actionDefault()
    {

        if($this->factoryDate->isNowAfterTicketShop()) {
            //is end of ticket shoping
            $this->redirect(301, "VideoForm:default");
        }
        $this->normalTicketForNow = $this->modelTicket->getClassicTicketForNow();
        $this->vipTicketForNow = $this->modelTicket->getVIP();
    }

    public function renderDefault()
    {
        $this->template->normalTicketForNow = $this->normalTicketForNow;
        $this->template->vipTicketForNow = $this->vipTicketForNow;

        $this->template->normalTicketForNowPrice = $this->convertPriceToPriceWithDiscount( $this->normalTicketForNow->price );
        $this->template->vipTicketForNowPrice = $this->convertPriceToPriceWithDiscount( $this->vipTicketForNow->price );


        ////////////////////////////////////////////////////////////////////////////////////////////
        $totalPrice = 0;
        if (isset($_POST["mainUser"])) {
            //ve formulari uz je neco vyplneno
            $totalPrice += $this->convertPriceToPriceWithDiscount($this->getTicketPrice($_POST["mainUser"]["ticket"]));
            if (isset($_POST["users"])) {
                foreach ($_POST["users"] as $userArray) {
                    //Přidat další jméno
                    //dump($userArray);exit;
                    if (isset($userArray["ticket"])) {
                        if (!isset($userArray["remove"])) {
                            //pouze pokud neni sloupecek remove (remove je ten co se maze)
                            $totalPrice += $this->convertPriceToPriceWithDiscount($this->getTicketPrice($userArray["ticket"]));
                        }
                    }
                }
                if (isset($_POST["users"]["add"])) {
                    //pridava se novy prvek
                    $totalPrice += $this->convertPriceToPriceWithDiscount($this->getTicketPrice(self::CLASSIC));
                }
            }

        } else {
            //ve formulari neni nic vyplneno
            if (isset($_GET["type"]) && ($_GET["type"] == "vip") ) {
                $totalPrice += $this->convertPriceToPriceWithDiscount($this->getTicketPrice(self::VIP));
            } else {
                $totalPrice += $this->convertPriceToPriceWithDiscount($this->getTicketPrice(self::CLASSIC));
            }
        }
        //dump($_POST);exit;
        $this->totalPriceTicket = $totalPrice;
        ////////////////////////////////////////////////////////////////////////////////////////////

        $this->template->totalPriceTicket = $this->totalPriceTicket;
        $this->template->discountRow = $this->discountRow;
    }

    public function renderFake()
    {
        $visitorRow = $this->modelVisitor->findOneBy(array("payments" => "payment_bank_transfer"));
        $this->redirect("Form:bankTransfer", array('token' => $visitorRow->token));
    }

    public function renderBankTransfer($token) {
        $visitorRow = $this->modelVisitor->findByToken($token);

        if($visitorRow != FALSE && $visitorRow->payments == "payment_bank_transfer") {
            $date = $visitorRow->registered;
            $date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti za 7 dni

            $this->template->maturityDate = $maturityDate;
            $this->template->visitorRow = $visitorRow;
        } else {
            Debugger::log("Pltaba kartou, token " . $token . " nenalezen v tabulce visitor", Debugger::EXCEPTION);
            $this->setView('paymentError');
        }
        //$this->setView('bankTransfer');*/
    }

    /**
     * Preved cenu na cenu se slevou
     * @param $price
     * @return mixed
     */
    private function convertPriceToPriceWithDiscount($price) {

        if($this->discountRow != null) {
            //celkovou cenu zaokrouhli nahoru
            return ($price - floor($price / 100 * $this->discountRow->percent ));
        } else {
            return $price;
        }
    }
    /**
     * Vrat hodnotu ticketu (classic/vip)
     * @param $ticketName
     * @return mixed|Nette\Database\Table\ActiveRow
     */
    private function getTicketPrice( $ticketName ) {
        if($ticketName== self::CLASSIC) {
            return $this->normalTicketForNow->price;
        } else if($ticketName== self::VIP) {
            return $this->vipTicketForNow->price;
        }
    }

	protected function createComponentTicketForm()
	{
        $form = new Nette\Application\UI\Form;

        $renderer = $form->getRenderer();
        $renderer->wrappers['label']['requiredsuffix'] = "*";

        $tickets = array(
            self::CLASSIC => Html::el()->setHtml("Běžná vstupenka <span title=\"Běžná vstupenka zahrnuje
  - Vstup po oba dny konference
  - Každý den oběd a občerstvení
  - Vstup na večerní párty\">?</span>"),
            self::VIP => Html::el()->setHtml("VIP vstupenka <span title=\"VIP vstupenka zahrnuje
  - Odbavení u vstupu do 1 min.
  - Sezení na nejlepších místech
  - Vlastní místo u stolu
  - Přípojka elektřiny (1x 230V)
  - Vstup po oba dny konference
  - Každý den oběd a občerstvení
  - Vstup na večerní párty\">?</span>"),
        );

        /////////////////////////////
        $getArray = $this->factoryContainerLocator->getHttpQuery();
        $type = self::CLASSIC;
        if(isset($getArray["type"])) {
            if($getArray["type"] == self::VIP) {
                $type = self::VIP;
            }
        }
        /////////////////////////////

        //////////////////////////////// start main user
        $mainUser = $form->addContainer('mainUser');
        $mainUser->addText('name', 'Jméno a příjmení:' )->setRequired("Zadejte jméno a příjmení");
        $mainUser->addText('company', 'Firma/pozice:')->setRequired("Zadejte firmu/pozici");
        $mainUser->addText('email', 'Váš e-mail:')->setType('email')->addRule(Form::EMAIL, 'Zadaný e-mail nemá správný formát')->setRequired("Zadejte Váš e-mail");
        $mainUser->addRadioList('ticket', '', $tickets)->setDefaultValue($type)->setAttribute('class', 'claculate-price')->setRequired("Zvolte druh vstupenky");
        //////////////////////////////// end main user

        $users = $form->addDynamic('users', function (Container $user) use ($tickets) {

            $user->addText('name', 'Jméno a příjmení:')->setRequired("Zadejte jméno a příjmení");
            $user->addText('company', 'Firma/pozice:')->setRequired("Zadejte firmu/pozici");
            $user->addText('email', 'E-mail:')->setType('email')->addRule(Form::EMAIL, 'Zadaný e-mail nemá správný formát')->setRequired("Zadejte e-mail");
            $user->addRadioList('ticket', '', $tickets)->setValue('classic')->setAttribute('class', 'claculate-price')->setRequired("Zvolte druh vstupenky");

            $user->addSubmit('remove', 'Odstranit toto jméno')
                ->setAttribute('class', 'ajax')
                ->addRemoveOnClick(function(){
                    if(isset($_POST["discount"]["discountPercent"]) && $_POST["discount"]["discountPercent"] != "") {
                        $this->discountRow = $this->modelDiscount->findActiveByName($_POST["discount"]["discountPercent"]);
                    }
                    if ($this->isAjax()) {
                        $this->redrawControl( 'ticketSnippet' );
                    }
                });

        }, 0);

        $users->addSubmit('add', 'Přidat další jméno')
            ->setValidationScope(FALSE)
            ->setAttribute('class', 'ajax')
            ->onClick[] = $this->addCallback;

        $users->addSubmit('discount', 'Uplatnit slevový kupon')
            ->setValidationScope(FALSE)
            ->setAttribute('class', 'ajax')
            ->setAttribute('id', 'discount-button')
            ->onClick[] = $this->addCallbackRefresh;

        //////////////////////////////// start fakturacni udaje
        $invoice = $form->addContainer('discount');
        $invoice->addText('discountPercent', 'Sleva:');

        $invoice = $form->addContainer('invoice');
        $invoice->addText('company', 'Jméno/firma:')->setRequired("Zadejte Vaše jméno / firmu");
        $invoice->addText('email', 'Email:')->setType('email')->addRule(Form::EMAIL, 'Zadaný e-mail nemá správný formát')->setRequired("Zadejte e-mail");
        $invoice->addText('ic', 'IČ:');
        $invoice->addText('street', 'Ulice:');
        $invoice->addText('city', 'Město:')->setRequired("Zadejte město");
        $invoice->addText('dic', 'DIČ:');
        $invoice->addText('number', 'ČP:')->setRequired("Zadejte ČP");
        $invoice->addText('zip', 'PSČ:')->setRequired("Zadejte PSČ");

        $invoice->addSelect('country', 'Stát:', $this->modelCountry->getFP());
        //////////////////////////////// end fakturacni udaje

        $payments = array(
            self::PAYMENT_CARD => Html::el()->setHtml("Platební karta <img src='/images/pay_card.png' alt='Podporované platební karty'>"),
            self::PAYMENT_BANK_TRANSFER => "Bankovní převod",
        );
        $form->addRadioList('payments', '', $payments)->setDefaultValue(self::PAYMENT_CARD);

        $form->addSubmit('send', 'Objednat');


        $form->onSuccess[] = $this->ticketFormSubmit;

        return $form;
	}

    public function addCallback(Nette\Forms\Controls\SubmitButton $button)
    {
        if(isset($_POST["discount"]["discountPercent"]) && $_POST["discount"]["discountPercent"] != "") {
            $this->discountRow = $this->modelDiscount->findActiveByName($_POST["discount"]["discountPercent"]);
        }
        $button->parent->createOne();
    }

    /**
     * Kliknuto na slevovy kupon
     * @param Nette\Forms\Controls\SubmitButton $button
     */
    public function addCallbackRefresh(Nette\Forms\Controls\SubmitButton $button)
    {
        if(isset($_POST["discount"]["discountPercent"]) && $_POST["discount"]["discountPercent"] != "") {
            $this->discountRow = $this->modelDiscount->findActiveByName($_POST["discount"]["discountPercent"]);
        }
    }

    public function ticketFormSubmit(Form $form, $values)
    {
        if ($this->isAjax()) {
            //echo "AJAX";exit;
            $this->redrawControl( 'ticketSnippet' );
        }
        if(!$form['send']->isSubmittedBy()) return;//zpracuj pouze pokud je odeslano tlacitkem objednat

        if(isset($_POST["discount"]["discountPercent"]) && $_POST["discount"]["discountPercent"] != "") {
            $this->discountRow = $this->modelDiscount->findActiveByName($_POST["discount"]["discountPercent"]);
        }

        //dump($values);exit;
        $totalPrice = 0;
        $totalFullPrice = 0;//cena bez slevy

        $visitorArray = array();
        $visitorArray["registered"] = new Nette\Utils\DateTime();
        $visitorArray["token"] = Random::generate(32);
        $visitorArray["invoice_company"] = $values["invoice"]["company"];
        $visitorArray["invoice_email"] = $values["invoice"]["email"];
        $visitorArray["invoice_ic"] = $values["invoice"]["ic"];
        $visitorArray["invoice_dic"] = $values["invoice"]["dic"];
        $visitorArray["invoice_street"] = $values["invoice"]["street"];
        $visitorArray["invoice_number"] = $values["invoice"]["number"];
        $visitorArray["invoice_city"] = $values["invoice"]["city"];
        $visitorArray["invoice_zip"] = $values["invoice"]["zip"];
        $visitorArray["country_id"] = $values["invoice"]["country"];

        $visitorArray["payments"] = $values["payments"];//zpusob platby

        if($this->discountRow) {
            $visitorArray["discount_id"] = $this->discountRow->id;//sleva
        }

        $visitorRow = $this->modelVisitor->insert($visitorArray);

        //////////////////////////////////////////////////////////////// start first ticket
        $mainTicketArray = array();
        $mainTicketArray["visitor_id"] = $visitorRow->id;
        $mainTicketArray["name"] = $values["mainUser"]["name"];
        $mainTicketArray["position"] = $values["mainUser"]["company"];
        $mainTicketArray["email"] = $values["mainUser"]["email"];
        $mainTicketArray["ticket_id"] = $this->getTicketIdFromName($values["mainUser"]["ticket"]);

        $this->modelVisitorTicket->insert($mainTicketArray);
        //////////////////////////////////////////////////////////////// end first ticket

        foreach($values["users"] as $user) {
            $visitorTicketArray = array();
            $visitorTicketArray["visitor_id"] = $visitorRow->id;
            $visitorTicketArray["name"] = $user["name"];
            $visitorTicketArray["position"] = $user["company"];
            $visitorTicketArray["email"] = $user["email"];
            $visitorTicketArray["ticket_id"] = $this->getTicketIdFromName($user["ticket"]);

            $this->modelVisitorTicket->insert($visitorTicketArray);
        }

        //celkova cena
        foreach($visitorRow->related('visitor_ticket.visitor_id') as $row) {
            $totalPrice+=$this->convertPriceToPriceWithDiscount($row->ticket->price);
            $totalFullPrice+=$row->ticket->price;
        }
        $visitorRow->update(array( "price" => $totalPrice, "discount_price" => ($totalFullPrice-$totalPrice) ));

        if($visitorRow->payments == "payment_bank_transfer") {

            //platba bankovnim prevodem, nastav variabilni symbol
            $visitorRow->update(array( "variable_symbol" => Factory\SettingsFactory::getUserVariableSymbol($visitorRow->id)));

            $date = new DateTime();
            $date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti za 7 dni

            $latte = new Latte\Engine;
            $params = array(
                'visitorRow' => $visitorRow,
                'maturityDate' => $maturityDate
            );

            try {
                $this->factoryEmail->sendEmail(NULL, $visitorRow->invoice_email, "Přijali jsme registrace – zbývá už jen uhradit | E-shop summit 2016", $latte->renderToString( "./../app/presenters/templates/email/bankTrasnfer.latte", $params));
            } catch (Nette\Mail\SmtpException $e) {
                $this->flashMessage( "Nepodařilo se Vám odeslat email s informacemi o platbě (".$e->getMessage().")", "alert-danger" );
                Debugger::log("Uživateli id: ".$visitorRow->id.", token: ".$visitorRow->token.", email: ".$visitorRow->invoice_email." se nepodařilo odeslat email při bankovnim prevodu: " . $e->getMessage(), Debugger::EXCEPTION);
            }

            $this->redirect("Form:bankTransfer", array('token' => $visitorRow->token));

            //$this->template->maturityDate = $maturityDate;
            //$this->template->visitorRow = $visitorRow;
            //$this->setView('bankTransfer');
        } else {

            //platba bankovnim prevodem, nastav variabilni symbol - Kali chce variabilni symbol na fakture i pri platbe kartou
            $visitorRow->update(array( "variable_symbol" => Factory\SettingsFactory::getUserVariableSymbol($visitorRow->id)));

            $date = new DateTime();
            $date->add(new DateInterval('P7D'));//7 dni
            $maturityDate = $date->format('d.m.Y');//datum splatnosti za 7 dni

            $latte = new Latte\Engine;
            $params = array(
                'visitorRow' => $visitorRow,
                'maturityDate' => $maturityDate,
                'payUrl' => $this->link("//Contribution:payment", $visitorRow->token)
            );

            try {
                $this->factoryEmail->sendEmail(NULL, $visitorRow->invoice_email, "Přijali jsme registrace – zbývá už jen uhradit | E-shop summit 2016", $latte->renderToString( "./../app/presenters/templates/email/cardTrasnfer.latte", $params));
            } catch (Nette\Mail\SmtpException $e) {
                $this->flashMessage( "Nepodařilo se Vám odeslat email s informacemi o platbě (".$e->getMessage().")", "alert-danger" );
                Debugger::log("Uživateli id: ".$visitorRow->id.", token: ".$visitorRow->token.", email: ".$visitorRow->invoice_email." se nepodařilo odeslat email při platbě kartou: " . $e->getMessage(), Debugger::EXCEPTION);
            }

            $this->redirect("Contribution:payment", array('token' => $visitorRow->token));
        }
    }

    private function getTicketIdFromName($ticketName) {
        if($ticketName == "vip") {
            //VIP vstupenka
            $ticketVipRow = $this->modelTicket->getVIP();
            return $ticketVipRow->id;
        } else {
            //klasicka vstupenka
            $ticketClassicRow = $this->modelTicket->getClassicTicketForNow();
            return $ticketClassicRow->id;
        }
    }

}
