<?php

namespace App\Presenters;
use App\Model;
use App\Model\PaymentTransaction;
use App\Model\VideoPaymentTransaction;
use Nette\Application\Responses\TextResponse;
use Nette\Http\IRequest;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Services;
use Services\ComGate;

/**
 * Callback po zaplaceni kartou
 * 4.       Brána posílá úspěšnou platbu na PaymentPresenter:: actionUpdateState(). Tato metoda aktualizuje stav platby, označí jako zaplacenou a vygeneruje fakturu v iDokladu: řeší to metoda InvoiceCreator::createInvoice(Backer $backer), a to ve dvou krocích – nejprve v iDokladu založí kontakt a poté vytvoří fakturu. Nakonec se faktura přes iDoklad uživateli pošle.
 * Class PaymentPresenter
 * @package App\Presenters
 */
class PaymentPresenter extends BasePresenter
{

    /** @var ComGate\Connector */
    private $paymentConnector;

    /** @var Model\Video */
    protected $modelVideo;

    /** @var Model\Visitor */
    protected $modelVisitor;

    /** @var Services\InvoiceCreator */
    public $invoiceCreator;

    /** @var Services\VisitorService */
    public $serviceVisitor;

    public function __construct(ComGate\Connector $paymentConnector,
                                Model\Video $modelVideo,
                                Model\Visitor $modelVisitor,
                                Services\InvoiceCreator $invoiceCreator,
                                Services\VisitorService $serviceVisitor)
    {
        parent::__construct();
        $this->paymentConnector = $paymentConnector;
        $this->modelVideo = $modelVideo;
        $this->modelVisitor = $modelVisitor;
        $this->invoiceCreator = $invoiceCreator;
        $this->serviceVisitor = $serviceVisitor;
    }

    /**
     * Brána posílá úspěšnou platbu na PaymentPresenter:: actionUpdateState().
     * Tato metoda aktualizuje stav platby, označí jako zaplacenou a vygeneruje fakturu v iDokladu:
     */
    public function actionUpdateState()
    {
        /** @var \Nette\Http\Request $req */
        $req = $this->getHttpRequest();
        if (!$req->isMethod(IRequest::POST)) {
            $this->sendCode(400, 'Invalid request.');
        }

        //////////////////////////////////////////////////////////////
        //kurva tohle ref ID furt votravuje, protoye ted bylo dano do stavu Cancelled a predtim bylo udelane v ostre brane
        //docasne
        $body = $req->getPost();
        if($body["refId"] == "8grzdo618na8ytbbo1wzss936wxkvzba" || $body["refId"] =="a1a5n6z1r20y2po3pay0f09sfhwpctg2" || $body["refId"] == "gjrej6tnw1au2zip3uw9yj0tmj9qjm9p"
            || $body["refId"] == "gjrej6tnw1au2zip3uw9yj0tmj9qjm9p" || $body["refId"] == "x14ce0mm6apbh5x2nl8p49jo0d0kophb" || $body["refId"] == "5b0tcc7c3i7s032r26ht3tz3wv6s7fxk"
            || $body["refId"] == "56e0uca7bhz0ls8bkgymeydeilwz0w89" || $body["refId"] == "uy8iqy5xswtoqii4v3jr4bfokwnaa68x"
            || $body["refId"] == "0neosgj51jjsefxux4r10syn1bwj3jja"
            || $body["refId"] == "7uot5uk82r1jf11lh49urwjjev1ghyoz"
            || $body["refId"] == "bkd2vvgnw05uc6k4mr4wshy4296rw8oq"
            || $body["refId"] == "ur2x7tbhgxfzjvwifaiiztvwg8nrhc8j"
            || $body["refId"] == "33eeqdl5zihgzjcms0ddv7cjgokb54wm"
            || $body["refId"] == "eo8gq5hsqljsrjdrli5ywe1uqnzk0fx7"
        ) {
            $this->sendCode(0, 'OK');
        }
        //////////////////////////////////////////////////////////////

        try {
            $state = $this->paymentConnector->parseState($req);

            $id = Arrays::get($state, 'refId', NULL);
            $state = Arrays::get($state, 'status', NULL);

            if (empty($id)) {
                $this->sendCode(1001, 'Invalid refId.');
            }

            $visitorRow = $this->modelVisitor->findByToken($id);
            $videoRow = $this->modelVideo->findByToken($id);

            if( $visitorRow != FALSE) {
                if ($visitorRow->related('payment_transaction.visitor_id')->count() == 0) {
                    $this->sendCode(1002, 'Specified ID is not associated with an active payment transaction.');
                }
                $stateConst = NULL;
                switch ($state) {
                    case 'PENDING':
                        $stateConst = PaymentTransaction::PENDING; break;
                    case 'PAID':
                        $stateConst = PaymentTransaction::PAID; break;
                    case 'CANCELLED':
                        $stateConst = PaymentTransaction::CANCELLED; break;
                    default:
                        $this->sendCode(1003, 'Invalid state: ' . $state);
                }

                foreach($visitorRow->related('payment_transaction.visitor_id') as $paymentTransactionRow) {
                    if($paymentTransactionRow->state == PaymentTransaction::PAID ) {
                        //set only if in DB is satate PENDING
                        $this->sendCode(1004, 'In db is now PAID! In DB is:' . $paymentTransactionRow->state . ', new state is: ' . $state);
                    }
                    $paymentTransactionRow->update( array( "date" => new DateTime(), "state" => $stateConst ) );
                }

                if($stateConst == PaymentTransaction::PAID) {
                    $visitorRow->update( array( "paid" => 1, "paid_date" => new DateTime() ) );
                    $this->serviceVisitor->emitPaidEvent($visitorRow);//vytvor fakturu a posli ji emailem uzivateli
                }

                $this->sendCode(0, 'OK');
            } else if( $videoRow != FALSE) {

                if ($videoRow->related('video_payment_transaction.video_id')->count() == 0) {
                    $this->sendCode(1002, 'Specified ID is not associated with an active payment transaction - video.');
                }
                $stateConst = NULL;
                switch ($state) {
                    case 'PENDING':
                        $stateConst = VideoPaymentTransaction::PENDING; break;
                    case 'PAID':
                        $stateConst = VideoPaymentTransaction::PAID; break;
                    case 'CANCELLED':
                        $stateConst = VideoPaymentTransaction::CANCELLED; break;
                    default:
                        $this->sendCode(1003, 'Invalid video state: ' . $state);
                }

                foreach($videoRow->related('video_payment_transaction.video_id') as $paymentTransactionRow) {
                    if($paymentTransactionRow->state == VideoPaymentTransaction::PAID ) {
                        //set only if in DB is satate PENDING
                        $this->sendCode(1004, 'Video: in db is now PAID! In DB is:' . $paymentTransactionRow->state . ', new state is: ' . $state);
                    }
                    $paymentTransactionRow->update( array( "date" => new DateTime(), "state" => $stateConst ) );
                }

                if($stateConst == PaymentTransaction::PAID) {
                    $videoRow->update( array( "paid" => 1, "paid_date" => new DateTime() ) );
                    //$this->serviceVisitor->emitPaidEvent($videoRow);//vytvor fakturu a posli ji emailem uzivateli
                    $this->invoiceCreator->createInvoiceVideo($videoRow);//vytvor fakturu a posli ji emailem uzivateli
                }

                $this->sendCode(0, 'OK');
            } else {
                if($state == 'CANCELLED') {
                    //platby ktere nemam v DB a jsou canneclled neresim
                    $this->sendCode(0, 'OK');
                } else {
                    $this->sendCode(1001, 'Invalid refId.');
                }
            }

        } catch (ComGate\ConnectorException $e) {
            $this->sendCode(1000, $e->getMessage());
        }

    }

    private function sendCode($code, $message)
    {
        $text = http_build_query([
            'code' => $code,
            'message' => $message
        ]);
        $response = $this->getHttpResponse();
        $response->setContentType('application/x-www-form-urlencoded; charset=utf-8');
        $this->sendResponse(new TextResponse($text));
    }
}