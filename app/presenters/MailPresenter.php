<?php

namespace App\Presenters;

use Nette;
use App\Factory;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Services\ComGate;
use Latte;


class MailPresenter extends BasePresenter
{
    /** @var Factory\EmailFactory */
    private $factoryEmail;

	public function __construct(Factory\EmailFactory $factoryEmail)
	{
        $this->factoryEmail = $factoryEmail;
	}

    public function actionDefault() {
        $this->factoryEmail->sendEmail(NULL, "vencax@gmail.com", "Proforma faktura - objednávka vstupenek", "text hehe pres smtp");
        echo "odelsano";
        exit;
    }

}
