<?php

namespace App\Presenters;

use App;
use App\Factory;
use App\Model;
use DateInterval;
use Nette;
use DateTime;
use Nette\Utils\Finder;
use Vencax;
use Exception;

class CronPresenter extends BasePresenter
{
    /** @var Factory\EmailFactory */
    public $factoryEmail;

    /** @var Model\User */
    public $modelUser;

    /** @var Model\Visitor */
    public $modelVisitor;

    public function __construct( Factory\EmailFactory $factoryEmail, Model\User $modelUser, Model\Visitor $modelVisitor )
    {
        $this->factoryEmail = $factoryEmail;
        $this->modelUser = $modelUser;
        $this->modelVisitor = $modelVisitor;
    }

    /**
     * Odesli email uzivatelim kteri maji proforma fakturu a jeste nezaplatili
     * Pokud uzivatel ma proforma fakturu a 7 dni od jeji vygenerovani nezaplatil, posli mu znovu email s proforma fakturu
     */
    public function actionBackupDb()
    {
        $o = "zaloha DB " . date("d.m.Y H:i:s") . "\n\n\n";

        $dbContext = $this->modelUser->getDbContext();
        //dump( $dbContext->query("SHOW TABLES") );

        $tables = $dbContext->query("SHOW TABLES")->fetchPairs();
        foreach( $tables as $tableName )
        {
            //dump( $tableName );
            $o.= "-- table: " . $tableName . "\n\n\n";

            $a = $dbContext->query( "SHOW CREATE TABLE " . $tableName )->fetchPairs();
            $o.= "\n\n" . $a[$tableName] . ";\n\n";

            $tableSelection = $dbContext->table( $tableName );
            foreach( $tableSelection as $tableRow )
            {
                $tableRowArray = $tableRow->toArray();
                $keys = array_keys( $tableRowArray );
                $values = array_values( $tableRowArray );

                foreach( $values as $index => $value )
                {
                    if(is_object($value) )
                    {
                        if( ( get_class( $value ) == "Nette\Utils\DateTime" ) )
                        {
                            $values[$index] = $value->format( "Y-m-d H:i:s" );
                        }
                        else if( get_class( $value ) == "DateInterval" )
                        {
                            $values[$index] = $value->format( "H:i:s" );
                        }
                        else
                        {
                            dump( $value );
                            echo "a kurva tenhle prvek neznam";
                            exit();
                        }
                    }
                }

                $values = $this->addApostrophesToArray( $values );

                //dump( $keys );
                //dump( $values );
                $o.= "INSERT INTO " . $tableName . " (" . implode( ", ", $keys ) . ") VALUES(" . implode( ", ", $values ) . ");\n";
            }
        }

        file_put_contents( "./backupdb/" . date("YmdHis") . ".sql", $o );
        echo "ok";
        //echo $o;
        $this->terminate();
    }

    /**
     * Urguj emailem lifi, kteri nemaji zaplaceno 7 dni a jeste jim nebyla odeslana notifikace
     */
    public function actionUrgenceEmail7Day() {
        $visitors = $this->modelVisitor->getVisitorsWithoutPayment7Days();
        foreach( $visitors as $visitorRow)
        {
            //tento uzivatel za posledni tyden nezaplatil proformu, posli mu urgenci a nastav ze mu bylo poslana urgence
            //echo "Odesilam urgenci na email: " . $visitorRow->invoice_email . "<br/>";

            $this->factoryEmail->sendUrgenceEmail7Day( $visitorRow, $this->link("//Contribution:payment", $visitorRow->token) );//odesli urgencni email

            //nastav uzivateli ze mu byla poslana urgence zaplaceni
            $visitorRow->update( array( "urgence_email_7_day" => new DateTime() ) );
        }

        echo "OK";

        $this->terminate();
    }

    /**
     * Pridej prvkum v poli apostrofy
     * @param $a
     * @return mixed
     */
    private function addApostrophesToArray( $a )
    {
        foreach( $a as $index => $value)
        {
            $a[$index] = "'" . $value . "'";
        }
        return $a;
    }

}
