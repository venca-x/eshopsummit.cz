<?php

namespace App\Presenters;

use Nette;
use App;
use App\Model;
use App\Factory;
use Nette\Application\BadRequestException;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /** @var App\Factory\DateFactory */
    protected $factoryDate;

    public function injectContainerx(App\Factory\DateFactory $factoryDate)
    {
        $this->factoryDate = $factoryDate;
    }

    public function startup()
    {
        parent::startup();

        $this->template->factoryDate = $this->factoryDate;
    }

    /**
     * Raises Error 404: Not Found.
     * @param string $message Message used in the exception.
     * @throws BadRequestException
     */
    public function raise404($message = NULL)
    {
        throw new BadRequestException($message);
    }

    /**
     * Pokud uzivatel neni prihlasen, presmeruj na titulni stranku
     */
    protected function mustBeAdminLogged()
    {
        if ( !$this->isAdmin() )
        {
            $this->redirect( ':Admin:Homepage:' );
        }
    }

    /**
     * Pokud uzivatel neni prihlasen, presmeruj na titulni stranku
     */
    protected function mustBeLogged()
    {
        if ( !$this->getUser()->isLoggedIn() )
        {
            $this->redirect( 'Homepage:' );
        }
    }

    /**
     * Je uyivatel admin?
     * @return bool
     */
    protected function isAdmin()
    {
        if ( $this->getUser()->isInRole('admin') == TRUE )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
}
