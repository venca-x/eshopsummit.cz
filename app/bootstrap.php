<?php

use Nette\Application\Routers\Route;

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

$secret = 'ssf8ghf1gf5';
$configurator->setDebugMode(array($secret."@188.120.209.117")); // enable for your remote IP
setcookie('nette-debug', $secret, strtotime('1 years'), '/', '', '', TRUE);

//pokud jsem na https, zobrazim https, jinak http
if( isset($_SERVER['HTTPS']) ) {
    Route::$defaultFlags = Route::SECURED;
}

$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
