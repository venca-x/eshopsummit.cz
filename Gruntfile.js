module.exports = function(grunt) {

	require( 'load-grunt-tasks' )( grunt, { scope: 'devDependencies' } );

    grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.initConfig({

		shell: {			
			composer_self_update: {
				command: 'composer self-update'
			},
			composer_update: {
				command: 'composer update'
			},
			bower_update: {
				command: 'bower update'
			},
			test: {
				command: 'vendor\\bin\\tester.bat -c tests/php-win.ini tests'
			},
			deploy_master: {
				command: 'cd deployment & deployment-master.bat',
                options: {
                    execOptions: {
                        maxBuffer: Infinity
                    }
                }
			},
			deploy_devel: {
				command: 'cd deployment & deployment-devel.bat',
                options: {
                    execOptions: {
                        maxBuffer: Infinity
                    }
                }
			}
		},
        watch: {
            scripts: {
                files: ['src/js/main.js'],
                tasks: ['dev-watch-js'],
                options: {
                    interrupt: true
                }
            },
            less: {
                files: [ 'src/less/styles.less' ],
                tasks: ['dev-watch-less'],
                options: {
                    livereload: true,
                },
            }
        },
        copy: {
            //copy files from bower_components to distribution
            bootstrapFonts: {
                expand: true,
                flatten: true,
                src: './bower_components/bootstrap/dist/fonts/*',
                dest: 'www/fonts'
            },
            bootstrap: {
                expand: true,
                cwd: './bower_components/bootstrap/dist/css',
                src: [
                    'bootstrap.min.css'
                ],
                dest: 'www/css/'
            },
            fontAwesome: {
                expand: true,
                flatten: true,
                src: './bower_components/font-awesome/fonts/*',
                dest: 'www/fonts'
            },
            jquery: {
                expand: true,
                cwd: './bower_components/jquery/dist',
                src: [
                    'jquery.min.js',
                    'jquery.min.map'
                ],
                dest: 'www/js/'
            },
        },
        less: {
            production: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    // target.css file: source.less file
                    "src/css/styles.css": ["src/less/styles.less"]
                }
            }
        },
        concat: {
            js: {
                src: ['bower_components/jquery/dist/jquery.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',//kurva do pici tooltip nejde
                    'bower_components/nette.ajax.js/nette.ajax.js',
                    'vendor/nette/forms/src/assets/netteForms.js',
                    'src/js/main.js'],
                dest: 'src/js/main.concat.js'
            },
            css: {
                src: ['src/css/styles.css', "bower_components/font-awesome/css/font-awesome.min.css"],
                dest: 'src/css/styles.concat.css'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            production: {
                files: {
                    'www/js/main.min.js': ['src/js/main.concat.js']
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'www/css/styles.min.css': ['src/css/styles.concat.css']
                }
            }
        }
	});

	grunt.registerTask('copy&compile', [ 'copy', 'less', 'concat', 'uglify', 'cssmin' ]);
	//grunt.registerTask('update', [ 'shell:composer_self_update', 'shell:composer_update', 'shell:bower_update', 'copy&compile', 'shell:test' ]);	//upgrade packages and deploy
	//grunt.registerTask('upgrade', [ 'update', 'shell:deploy_master' ]);	//upgrade packages and deploy
	//grunt.registerTask('deploy-master', [ 'copy&compile', 'shell:deploy_master' ]);
	grunt.registerTask('deploy-master', [ 'shell:deploy_master' ]);
	grunt.registerTask('deploy-devel', [ 'shell:deploy_devel' ]);
	grunt.registerTask('js-concat-uglify', [ 'concat:js', 'uglify:js' ]);
	grunt.registerTask('css-concat-minify', [ 'concat:css', 'cssmin' ]);
    grunt.registerTask('dev-watch-js', ['js-concat-uglify']);
    grunt.registerTask('dev-watch-less', ['less', 'css-concat-minify']);

};